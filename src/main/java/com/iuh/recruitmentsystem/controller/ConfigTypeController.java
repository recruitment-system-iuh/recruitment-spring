package com.iuh.recruitmentsystem.controller;

import com.iuh.recruitmentsystem.model.ApiResponse;
import com.iuh.recruitmentsystem.model.entity.ConfigType;
import com.iuh.recruitmentsystem.service.ConfigTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*",maxAge = 3600)
@RestController
@RequestMapping("/configtypes")
public class ConfigTypeController {

    @Autowired
    private ConfigTypeService  configTypeService;

    @GetMapping
    public ApiResponse<List<ConfigType>> listConfigType(){
        return new ApiResponse<>(HttpStatus.OK.value(), "ConfigType list fetched successfully.",configTypeService.findAll());
    }

//    @GetMapping("/{id}")
//    public ApiResponse<List<CustormConfigTypeDto>> listModelByID(@PathVariable long id){
//        return new ApiResponse<>(HttpStatus.OK.value(), "ConfigType list model json fetched successfully=.",configTypeService.listModelById(id));
//    }

}
