package com.iuh.recruitmentsystem.controller;

import com.iuh.recruitmentsystem.model.ApiResponse;
import com.iuh.recruitmentsystem.model.dto.PersonDto;
import com.iuh.recruitmentsystem.model.dto.UserDto;
import com.iuh.recruitmentsystem.model.entity.User;
import com.iuh.recruitmentsystem.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
@Api(value="User Management", description="Operations pertaining to users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ApiResponse<User> saveUser(@RequestBody UserDto user){
        System.out.println("user dto " + user.getUsername());
        return new ApiResponse<>(HttpStatus.OK.value(), "User saved successfully.",userService.save(user));
    }

    @GetMapping
    public ApiResponse<List<User>> listUser(){
        return new ApiResponse<>(HttpStatus.OK.value(), "User list fetched successfully.",userService.findAll());
    }

    @GetMapping("/{id}")
    public ApiResponse<User> getOne(@PathVariable long id){
        return new ApiResponse<>(HttpStatus.OK.value(), "User fetched successfully.",userService.findById(id));
    }

    @PutMapping("/{id}")
    public ApiResponse<UserDto> update(@RequestBody UserDto userDto,@PathVariable long id) {
        return new ApiResponse<>(HttpStatus.OK.value(), "User updated successfully.",userService.update(userDto,id));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable long id) {
        userService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "User fetched successfully.", null);
    }
    @GetMapping("username/{username}")
    public ApiResponse<User> getOne(@PathVariable String username){
        return new ApiResponse<>(HttpStatus.OK.value(), "User fetched successfully.",userService.loadUserIDByUsername(username));
    }
    @GetMapping("/rule/{rule}")
    public ApiResponse<List<User>> listUser(@PathVariable int rule){
        return new ApiResponse<>(HttpStatus.OK.value(), "User list fetched successfully.",userService.listUserByRule(rule));
    }

}
