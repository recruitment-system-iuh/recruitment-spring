package com.iuh.recruitmentsystem.controller;

import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.model.ApiResponse;
import com.iuh.recruitmentsystem.model.dto.CandidateDto;
import com.iuh.recruitmentsystem.model.dto.ResumeDto;
import com.iuh.recruitmentsystem.model.entity.Candidate;
import com.iuh.recruitmentsystem.service.CandidateService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/candidates")
@Api(value="Candidate Management", description="Operations pertaining to candidates")
public class CandidateController {

    @Autowired
    private CandidateService candidateService;

    @GetMapping
    public ApiResponse<List<Candidate>> listCandidates(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Candidate list fetched successfully.", candidateService.findAll());
    }

    @PutMapping("/{id}")
    public ApiResponse<CandidateDto> update(@RequestBody CandidateDto candidateDto, @PathVariable long id) throws InValidRollbackException {
        return new ApiResponse<>(HttpStatus.OK.value(),"Update Candidate Successful",candidateService.update(candidateDto,id));
    }

    @GetMapping("/{id}")
    public ApiResponse<Candidate> getOne(@PathVariable long id){
        return new ApiResponse<>(HttpStatus.OK.value(),"Get candidate by ID successful",candidateService.findCandidateByID(id));
    }

    @PostMapping("/applyCV")
    public ApiResponse<Candidate> applyCV(@RequestBody ResumeDto resumeDto) throws Exception {
        return  new ApiResponse<>(HttpStatus.OK.value(),"Apply Candidate Successful",candidateService.applyCV(resumeDto));
    }
}
