package com.iuh.recruitmentsystem.controller;

import com.iuh.recruitmentsystem.model.ApiResponse;
import com.iuh.recruitmentsystem.model.dto.InterviewCandiStatusDto;
import com.iuh.recruitmentsystem.model.dto.JobCandidateDto;
import com.iuh.recruitmentsystem.model.entity.JobCandidate;
import com.iuh.recruitmentsystem.model.hellper.CustormDataFeedbackDto;
import com.iuh.recruitmentsystem.model.hellper.CustormFeedbackDto;
import com.iuh.recruitmentsystem.model.hellper.CustormResumeJobCandiDto;
import com.iuh.recruitmentsystem.service.JobCandidateInterviewService;
import com.iuh.recruitmentsystem.service.JobCandidateService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/jobcandidates")
@Api(value="Job Management", description="Operations pertaining to jobs")
public class JobCandidateController {

    @Autowired
    private JobCandidateService jobCandidateService;

    @Autowired
    private JobCandidateInterviewService jobCandidateInterviewService;


    @GetMapping("/status/{status}")
    public ApiResponse<List<JobCandidateDto>> listJobCandidate(@PathVariable String status){

        System.out.println("Status " + status);
        List<JobCandidateDto> list =jobCandidateService.listJobCandidateByStatus(status);
        if(list.size() > 0)
            return new ApiResponse<>(HttpStatus.OK.value(), "List JobCandidate by status  " + status+" fetched successfully.", list);
        else
            return new ApiResponse<>(HttpStatus.NOT_FOUND.value(),"Status not found",null);
    }

    @GetMapping("/{id}")
    public ApiResponse<JobCandidate> getOne(@PathVariable long id){
        return new ApiResponse<>(HttpStatus.OK.value(),"Get JobCandidate by ID successful",jobCandidateService.findCandidateByID(id));
    }
    @GetMapping("/interview/{jobid}")
    public ApiResponse<List<InterviewCandiStatusDto>> listJobCandidate(@PathVariable long jobid){

        List<InterviewCandiStatusDto> list =jobCandidateService.listInterviewCandiStatusByJobID(jobid);
        if(list.size() > 0)
            return new ApiResponse<>(HttpStatus.OK.value(), "List InterviewCandiStatus by jobid  " + jobid+" fetched successfully.", list);
        else
            return new ApiResponse<>(HttpStatus.NOT_FOUND.value(),"jobid not found",null);
    }
    @GetMapping("/report")
    public ApiResponse<List<Integer>> reportJobCandiByOneMonth(){

        List<Integer> list =jobCandidateService.reportJobCandiByOneMonth();
        if(list.size() > 0)
            return new ApiResponse<>(HttpStatus.OK.value(), "Report Job Candidate in one Month", list);
        else
            return new ApiResponse<>(HttpStatus.NOT_FOUND.value(),"not found data",null);
    }
    @GetMapping("/feedback/{jobcanID}")
    public ApiResponse<CustormDataFeedbackDto> getdataFeedback(@PathVariable long jobcanID){
        return new ApiResponse<>(HttpStatus.OK.value(), "get data feednack successful", jobCandidateService.getFeedbackData(jobcanID));
    }
    @PutMapping("/feedback/{jobcanID}")
    public ApiResponse<CustormFeedbackDto> updateFeedback(@RequestBody CustormFeedbackDto custormFeedbackDto,@PathVariable long jobcanID){
        return new ApiResponse<>(HttpStatus.OK.value(), "get data feednack successful", jobCandidateInterviewService.updateFeedbackInterview(jobcanID,custormFeedbackDto));
    }
    @GetMapping("/review/{jobId}/{candId}")
    public ApiResponse<CustormResumeJobCandiDto> getDataReviewCandi(@PathVariable long jobId,@PathVariable long candId){
        System.out.println(jobId + " -- " + candId);
        return new ApiResponse<>(HttpStatus.OK.value(), "get data review successful", jobCandidateService.getReviewData(jobId,candId));
    }
    @PutMapping("/review/{jobcandID}")
    public ApiResponse<JobCandidate> updateJobCandWithStatusViewed(@PathVariable long jobcandID){
        return new ApiResponse<>(HttpStatus.OK.value(), "Update status viewed", jobCandidateService.updateJobCandidateWithStatusViewed(jobcandID));
    }

}
