package com.iuh.recruitmentsystem.controller;

import com.iuh.recruitmentsystem.model.ApiResponse;
import com.iuh.recruitmentsystem.model.dto.*;
import com.iuh.recruitmentsystem.model.entity.JobCandidate;
import com.iuh.recruitmentsystem.model.entity.JobCandidateInterview;
import com.iuh.recruitmentsystem.service.JobCandidateInterviewService;
import com.iuh.recruitmentsystem.service.JobCandidateService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/schedule")
@Api(value="Job Management", description="Operations pertaining to jobs")
public class JobCandidateInterviewController {

    @Autowired
    private JobCandidateInterviewService jobCandidateInterviewService;


    @GetMapping("/{jobcandID}")
    public ApiResponse<List<ScheduleDetailDto>> listScheduleDetailByJobCandiID(@PathVariable long jobcandID){

        List<ScheduleDetailDto> list =jobCandidateInterviewService.ListScheduleDetailByJobCandiID(jobcandID);
        if(list.size() > 0)
            return new ApiResponse<>(HttpStatus.OK.value(),
                    "List Schedule by jobcanID  " + jobcandID+" fetched successfully.", list);
        else
            return new ApiResponse<>(HttpStatus.NOT_FOUND.value(),"List Schedule not found",null);
    }

    @PostMapping
    public ApiResponse<ScheduleDto> saveSchedule(@RequestBody ScheduleDto schedule) throws ParseException {
        System.out.println("ScheduleDto " + schedule.toString());
        return new ApiResponse<>(HttpStatus.OK.value(), "Schedule saved successfully.",jobCandidateInterviewService.saveSchedule(schedule));
    }
    @GetMapping("/getone/{id}")
    public ApiResponse<JobCandidateInterviewDto> getOne(@PathVariable long id){
        return new ApiResponse<>(HttpStatus.OK.value(), "JobCandidateInterview fetched successfully.",jobCandidateInterviewService.findJobCadiIntByID(id));
    }

    @PutMapping("/{id}")
    public ApiResponse<JobCandidateInterview> update(@RequestBody String questionInterview,@PathVariable long id) {
        return new ApiResponse<>(HttpStatus.OK.value(), "JobCandidateInterview updated successfully.",jobCandidateInterviewService.updateQuestionJobCandiInt(id,questionInterview));
    }

}
