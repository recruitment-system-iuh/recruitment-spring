package com.iuh.recruitmentsystem.controller;

import com.iuh.recruitmentsystem.config.JwtTokenUtil;
import com.iuh.recruitmentsystem.model.ApiResponse;
import com.iuh.recruitmentsystem.model.AuthToken;
import com.iuh.recruitmentsystem.model.LoginUser;
import com.iuh.recruitmentsystem.model.entity.User;
import com.iuh.recruitmentsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
    public ApiResponse<AuthToken> register(@RequestBody LoginUser loginUser) throws AuthenticationException {
        System.out.println("login user");
        System.out.println(loginUser.getUsername() + " --" + loginUser.getPassword());
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUsername(), loginUser.getPassword()));
        System.out.println("11111");
        User user = userService.findOne(loginUser.getUsername());
        System.out.println("user");
        System.out.println(user.toString());
        final String token = jwtTokenUtil.generateToken(user);
        return new ApiResponse<>(200, "success",new AuthToken(token, user.getUsername()));
    }

}
