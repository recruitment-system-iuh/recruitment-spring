package com.iuh.recruitmentsystem.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.model.ApiResponse;
import com.iuh.recruitmentsystem.model.dto.InterviewerJobDto;
import com.iuh.recruitmentsystem.model.dto.JobDto;
import com.iuh.recruitmentsystem.model.entity.Job;
import com.iuh.recruitmentsystem.service.InterviewJobService;
import com.iuh.recruitmentsystem.service.JobService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/jobs")
@Api(value="Job Management", description="Operations pertaining to jobs")
public class JobsController {

    @Autowired
    private JobService jobService;

    @Autowired
    private InterviewJobService interviewJobService;

    @GetMapping
    public ApiResponse<List<Job>> listJobs(){
        return new ApiResponse<>(HttpStatus.OK.value(), "List All Job fetched successfully.",jobService.findAll());
    }
    @GetMapping("/listjob/{userid}/{jobid}")
    public ApiResponse<List<InterviewerJobDto>> listJobsByUserID(@PathVariable long userid, @PathVariable long jobid){
        return new ApiResponse<>(HttpStatus.OK.value(), "List all job by user_id fetched successfully.",interviewJobService.listJobByUserID(userid,jobid));
    }
    @PostMapping
    public ApiResponse<Job> saveJob(@RequestBody JobDto jobDto) throws InValidRollbackException, JsonProcessingException {
        System.out.println(jobDto.toString());
        return new ApiResponse<>(HttpStatus.OK.value(), "Job saved successfully.",jobService.save(jobDto));
    }
    @GetMapping("/{id}")
    public ApiResponse<Job> findJobByID(@PathVariable long id){
        return new ApiResponse<>(HttpStatus.OK.value(),"Find Job by ID successful",jobService.findById(id));
    }

}
