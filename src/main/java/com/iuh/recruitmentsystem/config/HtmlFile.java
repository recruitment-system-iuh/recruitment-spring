//package com.iuh.recruitmentsystem.config;
//
//import org.apache.pdfbox.exceptions.InvalidPasswordException;
//import org.apache.pdfbox.pdmodel.PDDocument;
//import org.apache.pdfbox.pdmodel.PDPage;
//import org.apache.pdfbox.pdmodel.common.PDStream;
//import org.apache.pdfbox.util.PDFTextStripper;
//import org.apache.pdfbox.util.TextPosition;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class HtmlFile extends PDFTextStripper {
//
//    private  String htmlFile;
//
//    private float zoom = (float) 2;
//    private int resolution = 72; //default resolution
//    private int lastMarginTop = 0;
//    private int lastMarginLeft = 0;
//    private int lastMarginRight = 0;
//    private int numberSpace = 0;
//    private int sizeAllSpace = 0;
//    private boolean addSpace;
//    private int startXLine;
//    private int lastFontSizePx = 0;
//
//    private StringBuffer currentLine = new StringBuffer();
//
//    private List<Map> listLine= new ArrayList<>();
//    public HtmlFile() throws IOException{
//
//    }
//
//    public List<Map> convertPdfToHtml(File pdfFile) throws Exception {
//        PDDocument document = null;
//        try {
//            document = PDDocument.load(pdfFile);
//
//            if(document.isEncrypted()){
//                try {
//                    document.decrypt( "" );
//                }
//                catch( InvalidPasswordException e ) {
//                    System.err.println( "Error: Document is encrypted with a password." );
//                    System.exit( 1 );
//                }
//            }
//            List allPages = document.getDocumentCatalog().getAllPages();
//
//            // Retrieve and save text in the HTML file
//            for( int i=0; i<allPages.size(); i++ ) {
//              //  System.out.println("Processing page "+i);
//                PDPage page = (PDPage)allPages.get(i);
//
//                PDStream contents = page.getContents();
//                if( contents != null ) {
//                    this.processStream( page, page.findResources(), page.getContents().getStream() );
//                }
//            }
//        }
//        finally {
//            if( document != null ) {
//                document.close();
//            }
//        }
//
//    return listLine;
//    }
//
//    /**
//     * A method provided as an event interface to allow a subclass to perform
//     * some specific functionality when text needs to be processed.
//     *
//     * @param text The text to be processed
//     */
//    protected void processTextPosition(TextPosition text )
//    {
//        try {
//            int marginLeft = (int)((text.getXDirAdj())*zoom);
//            int fontSizePx = Math.round(text.getFontSizeInPt()/72*resolution*zoom);
//            int marginTop = (int)((text.getYDirAdj())*zoom-fontSizePx);
//            renderingGroupByLineWithCache(text, marginLeft,  marginTop, fontSizePx);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//    private void renderingGroupByLineWithCache(TextPosition text, int marginLeft, int marginTop, int fontSizePx) throws IOException {
//
//        if (marginLeft-lastMarginRight> text.getWidthOfSpace()) {
//            currentLine.append(" ");
//
//            sizeAllSpace += (marginLeft-lastMarginRight);
//            numberSpace++;
//            addSpace = false;
//
//        }
//        if ((lastMarginTop != marginTop) || (lastFontSizePx != fontSizePx) || (lastMarginLeft>marginLeft) || (marginLeft-lastMarginRight>150)){
//            if (lastMarginTop != 0) {
//                boolean display = true;
//
//                // if the bloc is empty, we do not display it (for a lighter result)
//                if (currentLine.length() == 1) {
//                    char firstChar = currentLine.charAt(0);
//                    if (firstChar == ' ') {
//                        display = false;
//                    }
//                }
//                if (display) {
//                    Map<String,String> hmLine = new HashMap<String, String>();
//                    hmLine.put("line",currentLine.toString());
//                    hmLine.put("size",lastFontSizePx+"");
//                    listLine.add(hmLine);
//                }
//            }
//            numberSpace = 0;
//            sizeAllSpace = 0;
//
//            currentLine = new StringBuffer();
//            startXLine = marginLeft;
//            lastMarginTop = marginTop;
//            lastFontSizePx = fontSizePx;
//
//            addSpace = false;
//        }
//        else {
//            int sizeCurrentSpace = (int) (marginLeft-lastMarginRight-text.getWidthOfSpace());
//            if (sizeCurrentSpace > 5) {
//                addSpace = false;
//            }
//            else {
//                if (addSpace) {
//                    currentLine.append(" ");
//
//                    sizeAllSpace += (marginLeft-lastMarginRight);
//                    numberSpace++;
//                    addSpace = false;
//                }
//
//            }
//        }
//
//        if (text.getCharacter().equals(" ")) {
//            addSpace = true;
//            //sizeAllSpace += text.getWidthOfSpace();
//        }
//        else {
//            currentLine.append(text.getCharacter().replace("<", "&lt;").replace(">", "&gt;"));
//        }
//        lastMarginLeft = marginLeft;
//        lastMarginRight = (int) (marginLeft + text.getWidth()*zoom);
//
//    }
//}
