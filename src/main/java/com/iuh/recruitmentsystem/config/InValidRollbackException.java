package com.iuh.recruitmentsystem.config;

public class InValidRollbackException extends Exception{

    public InValidRollbackException(String message) {
        super(message);
    }
}
