package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.model.entity.JobConfig;

import java.util.List;

public interface JobConfigService {


    List<Object[]> findAllByJobID(long job_id);

    JobConfig findById(long id);


}
