package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.model.dto.PersonDto;
import com.iuh.recruitmentsystem.model.entity.User;
import com.iuh.recruitmentsystem.model.dto.UserDto;

import java.util.List;

public interface UserService {

    User save(UserDto user);
    List<User> findAll();

    void delete(long id);

    User findOne(String username);

    User findById(long id);

    UserDto update(UserDto userDto, long id);

    User loadUserIDByUsername(String username);

    List<User> listUserByRule(int rule);
}
