package com.iuh.recruitmentsystem.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.model.dto.JobDto;
import com.iuh.recruitmentsystem.model.entity.Job;

import java.util.List;

public interface JobService {

    Job save(JobDto job) throws InValidRollbackException, JsonProcessingException;

    List<Job> findAll();

    void delete(long id);

    Job findById(long id);

    JobDto update(JobDto jobDto);

}
