package com.iuh.recruitmentsystem.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.model.dto.CandidateDto;
import com.iuh.recruitmentsystem.model.dto.ResumeDto;
import com.iuh.recruitmentsystem.model.entity.Candidate;

import java.util.List;

public interface CandidateService {

    Candidate findCandidateByID(long id);
    CandidateDto update(CandidateDto candidateDto,long id) throws InValidRollbackException;
    Candidate save(CandidateDto candidateDto);
    List<Candidate> findAll();
    Candidate applyCV(ResumeDto resumeDto) throws InValidRollbackException, JsonProcessingException;
    List<Object[]> getResumeDetailByID(long candidateID);
}
