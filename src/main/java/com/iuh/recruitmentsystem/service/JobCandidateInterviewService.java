package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.model.dto.JobCandidateInterviewDto;
import com.iuh.recruitmentsystem.model.dto.ScheduleDetailDto;
import com.iuh.recruitmentsystem.model.dto.ScheduleDto;
import com.iuh.recruitmentsystem.model.entity.JobCandidateInterview;
import com.iuh.recruitmentsystem.model.hellper.CustormFeedbackDto;

import java.text.ParseException;
import java.util.List;

public interface JobCandidateInterviewService {

    List<ScheduleDetailDto> ListScheduleDetailByJobCandiID(long jobcandID);

    ScheduleDto saveSchedule(ScheduleDto scheduleDto) throws ParseException;

    JobCandidateInterviewDto findJobCadiIntByID(long id);

    JobCandidateInterview updateQuestionJobCandiInt(long id, String question);

    JobCandidateInterview save(JobCandidateInterview jobCandidateInterview);

    JobCandidateInterview updateFeedbackInterview(long jobcanID, CustormFeedbackDto custormFeedbackDto);
}

