package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.model.dto.PersonDto;
import com.iuh.recruitmentsystem.model.entity.Person;

import java.util.List;

public interface PersonService {

    Person save(PersonDto person);

    List<Person> findAll();

    void delete(long id);

    Person findById(long id);

    Person update(PersonDto PersonDto,long id);
}
