package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.dao.LogDao;
import com.iuh.recruitmentsystem.dao.LogDetailDao;
import com.iuh.recruitmentsystem.model.dto.LogDetailDto;
import com.iuh.recruitmentsystem.model.entity.Candidate;
import com.iuh.recruitmentsystem.model.entity.Log;
import com.iuh.recruitmentsystem.model.entity.LogDetail;
import com.iuh.recruitmentsystem.service.LogService;
import com.iuh.recruitmentsystem.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service(value = "logService")
public class LogServiceImpl implements LogService {

    @Autowired
    private LogDao logDao;

    @Autowired
    private LogDetailDao logDetailDao;
    @Autowired
    private UserService userService;

    @Transactional(rollbackFor = InValidRollbackException.class)
    @Override
    public Log save(List<LogDetailDto> logDetails, Candidate candidate, long user_id) throws  InValidRollbackException{
        Log log = new Log();
        log.setCandidate_log(candidate);
        log.setUser_log(userService.findById(user_id));
        log = logDao.save(log);
        for( LogDetailDto log1: logDetails){
            LogDetail logDetail = new LogDetail();
            BeanUtils.copyProperties(log1, logDetail );
            System.out.println(logDetail.toString());
            logDetail.setLogs(log);
            logDetailDao.save(logDetail);
        }
        return log;
    }

    @Override
    public List<Log> listLogByTimeline(LogDetailDto logDto) {
        return null;
    }

    @Override
    public List<Log> findAllByCandidateID() {
        List<Log> list = new ArrayList<>();
        logDao.findAllByCandidateID(1).iterator().forEachRemaining(list::add);
        return list;
    }
}
