package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.dao.ConfigTypeDao;
import com.iuh.recruitmentsystem.model.hellper.CustormConfigTypeDto;
import com.iuh.recruitmentsystem.model.entity.ConfigType;
import com.iuh.recruitmentsystem.service.ConfigTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "configTypeService")
public class ConfigTypeServiceImpl implements ConfigTypeService {


    @Autowired
    private ConfigTypeDao configTypeDao;

    @Override
    public List<ConfigType> findAll() {
        List<ConfigType> list = new ArrayList<>();
        configTypeDao.findAll().iterator().forEachRemaining(list::add);
        return list;

    }

    @Override
    public List<CustormConfigTypeDto> listModelById(long id) {
        List<CustormConfigTypeDto> list = new ArrayList<>();
        configTypeDao.getModelById(id).iterator().forEachRemaining(list::add);
        return list;
    }
}
