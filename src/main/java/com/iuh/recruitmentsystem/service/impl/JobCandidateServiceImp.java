package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.dao.JobCandidateDao;
import com.iuh.recruitmentsystem.model.dto.InterviewCandiStatusDto;
import com.iuh.recruitmentsystem.model.dto.JobCandidateDto;
import com.iuh.recruitmentsystem.model.entity.Job;
import com.iuh.recruitmentsystem.model.entity.JobCandidate;
import com.iuh.recruitmentsystem.model.entity.JobCandidateInterview;
import com.iuh.recruitmentsystem.model.hellper.CustormDataFeedbackDto;
import com.iuh.recruitmentsystem.model.hellper.CustormJobConfig;
import com.iuh.recruitmentsystem.model.hellper.CustormResumeDetail;
import com.iuh.recruitmentsystem.model.hellper.CustormResumeJobCandiDto;
import com.iuh.recruitmentsystem.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.Reader;
import java.sql.Clob;
import java.util.*;

@Service(value = "jobCandidateService")
public class JobCandidateServiceImp implements JobCandidateService {

    @Autowired
    private JobCandidateDao jobCandidateDao;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private JobConfigService jobConfigService;

    @Autowired
    private JobService jobService;

    @Autowired
    private JobCandidateInterviewService jobCandidateInterviewService;

    @Override
    public List<JobCandidateDto> listJobCandidateByStatus(String status) {
        List<JobCandidateDto> list = new ArrayList<>();
        List<JobCandidate> candidateList = jobCandidateDao.findJobCandidatesByStatus(status);
        for(JobCandidate jobcandi: candidateList){
            JobCandidateDto jobCandidateDto = new JobCandidateDto();
            jobCandidateDto.setId(jobcandi.getId());
            jobCandidateDto.setCandidateID(jobcandi.getCandidate().getId());
            jobCandidateDto.setCreateDateTime(jobcandi.getCreateDateTime());
            jobCandidateDto.setCurrentRound(jobcandi.getCurrentRound());
            jobCandidateDto.setJobID(jobcandi.getJob_candidate().getId());
        //    jobCandidateDto.setIdInterviewRound(jobcandi.getIdInterviewRound());
            jobCandidateDto.setStatus(jobcandi.getStatus());
          //  jobCandidateDto.setStatusInterview(jobcandi.getStatusInterview());
            jobCandidateDto.setUrlCV(jobcandi.getUrlCV());
            jobCandidateDto.setPosition(jobcandi.getPosition());
            jobCandidateDto.setJobName(jobcandi.getJob_candidate().getJobName());
            jobCandidateDto.setCandidateName(jobcandi.getCandidate().getPerson().getFirstName()+ " " + jobcandi.getCandidate().getPerson().getLastName());
            list.add(jobCandidateDto);
        }
        return list;
    }

    @Override
    @Transactional(rollbackFor = InValidRollbackException.class)
    public JobCandidate updateJobCandidateWithStatusViewed(long jobcandidateId) {

        JobCandidate  jobCandidate = findCandidateByID(jobcandidateId);
        JobCandidateInterview jobcandInterview = new JobCandidateInterview();
        jobcandInterview.setStatus("wait");
        jobcandInterview.setRound(1);
        jobcandInterview.setJobCandidate(jobCandidate);
        jobcandInterview = jobCandidateInterviewService.save(jobcandInterview);
        jobCandidate.setStatus("viewed");
        jobCandidate.setCurrentRound(jobcandInterview.getId());
        return jobCandidateDao.save(jobCandidate);
    }


    @Override
    public JobCandidateDto updateIdInterviewRoundJobCandidate(long jobcandidateId, long userid) {
        return null;
    }

    @Override
    public JobCandidate updateCurrentRoundJobCandidate(JobCandidate jobcandidate, long currentRound) {
        jobcandidate.setCurrentRound(currentRound);
        jobCandidateDao.save(jobcandidate);
        return jobcandidate;
    }

    @Override
    public List<JobCandidate> listJobCandidateByStatusAndJobId(String status, long id) {
        return null;
    }

    @Override
    public JobCandidate findCandidateByID(long id) {
        Optional<JobCandidate> OptionalCandidate = jobCandidateDao.findById(id);
        return OptionalCandidate.isPresent() ? OptionalCandidate.get() : null;
    }

    @Override
    public List<InterviewCandiStatusDto> listInterviewCandiStatusByJobID(long jobid) {

        List<InterviewCandiStatusDto> list = new ArrayList<>();
        jobCandidateDao.findInterviewCandiByJobID(jobid).forEach( custormJobCandidateScheduleByID -> {
            InterviewCandiStatusDto interviewCandiStatusDto = new InterviewCandiStatusDto();
            interviewCandiStatusDto.setId(custormJobCandidateScheduleByID.getID());
            interviewCandiStatusDto.setJobcanID(custormJobCandidateScheduleByID.getJOBCANDID());
            interviewCandiStatusDto.setJobname(custormJobCandidateScheduleByID.getJOBNAME());
            interviewCandiStatusDto.setPosition(custormJobCandidateScheduleByID.getPOSITION());
            interviewCandiStatusDto.setJobRound(custormJobCandidateScheduleByID.getJOBROUND());
            interviewCandiStatusDto.setNumberRecruitment(custormJobCandidateScheduleByID.getNUMBERRECRUITMENT());
            interviewCandiStatusDto.setCandidateName(custormJobCandidateScheduleByID.getFIRSTNAME()+ " " + custormJobCandidateScheduleByID.getLASTNAME());
            interviewCandiStatusDto.setEmail(custormJobCandidateScheduleByID.getEMAIL());
            interviewCandiStatusDto.setPhoneNumber(custormJobCandidateScheduleByID.getPHONENUMBER());
            interviewCandiStatusDto.setCreatedDate(custormJobCandidateScheduleByID.getCREATEDATETIME());
            interviewCandiStatusDto.setJobcandiStatus(custormJobCandidateScheduleByID.getJOBCANDSTATUS());
            String statusInterview = custormJobCandidateScheduleByID.getSTATUS();
            if(statusInterview.equals("pass")){
                interviewCandiStatusDto.setInterviewRound(custormJobCandidateScheduleByID.getINTERVIEWROUND() + 1);
                interviewCandiStatusDto.setStatusInterview("waiting");
            }else{
                interviewCandiStatusDto.setInterviewRound(custormJobCandidateScheduleByID.getINTERVIEWROUND());
                interviewCandiStatusDto.setStatusInterview(statusInterview +"ing");
            }
            list.add(interviewCandiStatusDto);
        });
        return list;
    }

    @Override
    public List<Integer> reportJobCandiByOneMonth() {
        List<Integer> list = new ArrayList<>();
        jobCandidateDao.reportJobCandiByOneMonth().forEach( report -> {
            list.add(report.getTotal());
            list.add(report.getWaited());
            list.add(report.getViewed());
            list.add(report.getInterview());
            list.add(report.getOffer());
            list.add(report.getJobnew());
        });
        return list;
    }

    @Override
    public CustormDataFeedbackDto getFeedbackData(long jobcandID) {
        CustormDataFeedbackDto feedbackDto= null;
        List<Object[]> result = jobCandidateDao.getdataFeedback(jobcandID);
        if(result != null && !result.isEmpty()){
            for (Object[] object : result) {
                feedbackDto = new CustormDataFeedbackDto();
                feedbackDto.setJobName(object[0].toString());
                feedbackDto.setP_Recruitment(object[1].toString());
                feedbackDto.setName(object[2].toString() + " " + object[3].toString());
                feedbackDto.setEmail(object[4].toString());
                feedbackDto.setP_apply(object[5].toString());
                feedbackDto.setQuestionInterview(convertClobToString((Clob)object[6]));
                feedbackDto.setUrlCV(object[7].toString());
                feedbackDto.setRound(Integer.parseInt(object[8].toString()));
                feedbackDto.setAddress(object[9].toString());
                feedbackDto.setDate_Interview(object[10].toString());
            }
        }
        return feedbackDto;
    }

    @Override
    public JobCandidate save(JobCandidate jobCandidate) {
        return jobCandidateDao.save(jobCandidate);
    }

    @Override
    public CustormResumeJobCandiDto getReviewData(long jobID, long candId) {
        CustormResumeJobCandiDto resumeJobCandiDto = new CustormResumeJobCandiDto();
        Job job = jobService.findById(jobID);
        resumeJobCandiDto.setJobName(job.getJobName());
        resumeJobCandiDto.setPosition(job.getPosition());
        resumeJobCandiDto.setJobDescription(job.getJobDescription());
        resumeJobCandiDto.setJobRequirement(job.getJobRequirement());
        List<Object[]> candidateResume = candidateService.getResumeDetailByID(candId);
        List<CustormResumeDetail> listResumeDetails = new ArrayList<>();
        if(candidateResume != null && !candidateResume.isEmpty()){
            for (Object[] object : candidateResume) {
                CustormResumeDetail resumeDetail = new CustormResumeDetail();
                resumeDetail.setHeader(object[0].toString());
                if(object[1]!=null) {
              //      System.out.println(object[1].toString()); // SubHeader
                    resumeDetail.setSubHeader(object[1].toString());
                }
                resumeDetail.setContent(object[2].toString()); // Content
                listResumeDetails.add(resumeDetail);
            }
        }
        List<Object[]> jobconfigDetail = jobConfigService.findAllByJobID(jobID);
        List<CustormJobConfig> listJobConfig = new ArrayList<>();
        if(jobconfigDetail != null && !jobconfigDetail.isEmpty()){
            for (Object[] object : jobconfigDetail) {
                CustormJobConfig jobConfig = new CustormJobConfig();
                jobConfig.setContent(convertClobToString((Clob)object[0]));
                jobConfig.setConfigTypeID(Long.parseLong(object[1].toString()));
                jobConfig.setConfigName(object[2].toString());
                jobConfig.setConfigTypeName(object[3].toString());
                listJobConfig.add(jobConfig);
            }
        }
        resumeJobCandiDto.setResumeDetais(listResumeDetails);
        resumeJobCandiDto.setJobConfigs(listJobConfig);
        return resumeJobCandiDto;
    }

    public String convertClobToString(Clob clob) {
        String result = "";
        final StringBuilder sb = new StringBuilder();
        try {
            Reader reader = clob.getCharacterStream();
            BufferedReader br = new BufferedReader(reader);
            int b;
            while (-1 != (b = br.read())) {
                sb.append((char) b);
            }
            br.close();
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
