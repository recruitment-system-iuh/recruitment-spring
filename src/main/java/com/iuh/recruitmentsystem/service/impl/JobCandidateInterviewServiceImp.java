package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.dao.InterviewJobDao;
import com.iuh.recruitmentsystem.dao.JobCandidateDao;
import com.iuh.recruitmentsystem.dao.JobCandidateInterviewDao;
import com.iuh.recruitmentsystem.model.dto.JobCandidateInterviewDto;
import com.iuh.recruitmentsystem.model.dto.ScheduleDetailDto;
import com.iuh.recruitmentsystem.model.dto.ScheduleDto;
import com.iuh.recruitmentsystem.model.entity.InterviewerJob;
import com.iuh.recruitmentsystem.model.entity.JobCandidate;
import com.iuh.recruitmentsystem.model.entity.JobCandidateInterview;
import com.iuh.recruitmentsystem.model.entity.User;
import com.iuh.recruitmentsystem.model.hellper.CustormFeedbackDto;
import com.iuh.recruitmentsystem.service.JobCandidateInterviewService;
import com.iuh.recruitmentsystem.service.JobCandidateService;
import com.iuh.recruitmentsystem.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service(value = "jobCandidateInterviewService")
public class JobCandidateInterviewServiceImp implements JobCandidateInterviewService {

    @Autowired
    private JobCandidateInterviewDao jobCandidateInterviewDao;

    @Autowired
    private UserService userService;

    @Autowired
    private JobCandidateService jobCandidateService;

    @Autowired
    private InterviewJobDao interviewJobDao;

    @Autowired
    private JobCandidateDao jobCandidateDao;

    @Override
    public List<ScheduleDetailDto> ListScheduleDetailByJobCandiID(long jobcandID) {
        List<ScheduleDetailDto> list = new ArrayList<>();
        jobCandidateInterviewDao.findScheduleDetailByJobCandiID(jobcandID).forEach(custormScheduleDetailDto -> {
            ScheduleDetailDto scheduleDetailDto = new ScheduleDetailDto();
            scheduleDetailDto.setJobcanId(custormScheduleDetailDto.getId());
            scheduleDetailDto.setRound(custormScheduleDetailDto.getROUND());
            scheduleDetailDto.setStatus(custormScheduleDetailDto.getSTATUS());
            scheduleDetailDto.setDateTime(custormScheduleDetailDto.getDATETIME());
            scheduleDetailDto.setFeedback(custormScheduleDetailDto.getFEEDBACK());
            scheduleDetailDto.setAddress(custormScheduleDetailDto.getADDRESS());
            scheduleDetailDto.setUserFeedbackName(custormScheduleDetailDto.getFIRSTNAME() + " " + custormScheduleDetailDto.getLASTNAME());
            list.add(scheduleDetailDto);
        });
        return list;
    }

    /*
    * Tạo 1 lịch phỏng vấn cho các Candidate đang ở trạng thái interviewing Và trạng thái phỏng vấn là waiting
    * Đối với Candidate đó ai sẽ là người feed back.
     *Ai sẽ là Supporter -> Insert JobInterview để phân quyền Interviewer có thể xem được thông tin của JobCandi đó không
     * Vừa phần quyền Job vừa phân quyền JobCandidate
     * 1 Interviewer sẽ thấy được những job mà mình tham gia. Và những Candidate mà mình được quyền supporter
    * */
    @Override
    @Transactional(rollbackFor = InValidRollbackException.class)
    public ScheduleDto saveSchedule(ScheduleDto scheduleDto) throws ParseException {

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date dateTime = df.parse(scheduleDto.getTime());

        for(String s_jobcanID: scheduleDto.getJobcandIDs()){
            long jobcanID = Long.parseLong(s_jobcanID);
            JobCandidate jobCandidate = jobCandidateService.findCandidateByID(jobcanID);
            System.out.println(jobCandidate.getStatus());
            // supporter
            for(int s_userID: scheduleDto.getSupporters()){
                long supporterID = s_userID;
                InterviewerJob  supporterJob = new InterviewerJob();
                User supporter = userService.findById(supporterID);
                // rule =2 => Supporter
                supporterJob.setRule(2);
                supporterJob.setJobCandidate(jobCandidate);
                supporterJob.setInterview_job(supporter);
                interviewJobDao.save(supporterJob);
                System.out.println(supporterJob.toString());
            }

            String name = jobCandidate.getCandidate().getPerson().getLastName();
            JobCandidateInterview jobCandidateInterview = jobCandidateInterviewDao.findById(jobCandidate.getCurrentRound()).get();
            int round = (int)jobCandidateInterview.getRound();
            String status = jobCandidateInterview.getStatus();
            boolean jobcandiIsViewedStauts = false;
            // Tạo lịch phỏng vấn đối với các Interview có statusInterview waited. Tức là vòng 1 pass -> vòng 2 interview
            if(round == 1 && status.equalsIgnoreCase("wait")){
                // Update JobCandidate with status viewed
                jobCandidateInterview.setAddress(scheduleDto.getAddress());
                jobCandidateInterview.setDateTime(dateTime);
                jobCandidateInterview.setStatus("interview");
                jobCandidateInterviewDao.save(jobCandidateInterview);
                jobCandidate.setStatus("interview");
                jobCandidateDao.save(jobCandidate);
                jobcandiIsViewedStauts = true;
            }else {

                // Insert jobcandidateInterview
                JobCandidateInterview interview = new JobCandidateInterview();
                interview.setAddress(scheduleDto.getAddress());
                interview.setDateTime(dateTime);
                interview.setRound(round + 1);
                interview.setStatus("interview");
                interview.setJobCandidate(jobCandidate);
                JobCandidateInterview interviewSave = jobCandidateInterviewDao.save(interview);
                jobCandidateService.updateCurrentRoundJobCandidate(jobCandidate,interviewSave.getId());
            }
//            jobCandidate.setCurrentRound(interviewSave.getId());
            // interviewer
            long interviewerID = scheduleDto.getInterviewer();
            User interviewer = userService.findById(interviewerID);
            InterviewerJob  interviewerJob = new InterviewerJob();
            interviewerJob.setRule(1);
            interviewerJob.setJobCandidate(jobCandidate);
            interviewerJob.setInterview_job(interviewer);
            if (jobcandiIsViewedStauts){
                interviewerJob.setRound(round);
            }else {
                interviewerJob.setRound(round + 1);
            }
            interviewJobDao.save(interviewerJob);
            System.out.println(interviewerJob.toString());
        }
        return null;
    }

    @Override
    public JobCandidateInterviewDto findJobCadiIntByID(long id) {

        JobCandidateInterview jobCandidateInterview = findByID(id);
        JobCandidateInterviewDto jobCandidateInterviewDto = null;
        if(jobCandidateInterview !=null){
            jobCandidateInterviewDto = new JobCandidateInterviewDto();
            BeanUtils.copyProperties(jobCandidateInterview, jobCandidateInterviewDto);
        }
        return jobCandidateInterviewDto;
    }
    public JobCandidateInterview findByID(long id) {
        Optional<JobCandidateInterview> optionalJobCandidateInterview = jobCandidateInterviewDao.findById(id);
        return optionalJobCandidateInterview.isPresent() ? optionalJobCandidateInterview.get() : null;
    }
    @Override
    public JobCandidateInterview updateQuestionJobCandiInt(long id, String question) {
        JobCandidateInterview jobCandidateInterview = findByID(id);
        if(jobCandidateInterview !=null){
            jobCandidateInterview.setQuestionInterview(question);
            jobCandidateInterviewDao.save(jobCandidateInterview);
        }
        return jobCandidateInterview;
    }

    @Override
    public JobCandidateInterview save(JobCandidateInterview jobCandidateInterview) {
        return jobCandidateInterviewDao.save(jobCandidateInterview);
    }

    @Override
    public JobCandidateInterview updateFeedbackInterview(long jobcanID, CustormFeedbackDto custormFeedbackDto) {
        JobCandidate jobCandidate = jobCandidateService.findCandidateByID(jobcanID);
        JobCandidateInterview jobCandidateInterview = new JobCandidateInterview();
        jobCandidateInterview = findByID(jobCandidate.getCurrentRound());
        jobCandidateInterview.setFeedback(custormFeedbackDto.getFeedback());
        jobCandidateInterview.setAnswerInterview(custormFeedbackDto.getAnswerInterview());
        if(custormFeedbackDto.getStatus() == 1){
            // pass round interview
            jobCandidateInterview.setStatus("pass");
            jobCandidateInterviewDao.save(jobCandidateInterview);
        }else if(custormFeedbackDto.getStatus() == 2){
            // offer
            jobCandidateInterview.setStatus("offer");
            jobCandidateInterviewDao.save(jobCandidateInterview);
            jobCandidate.setStatus("offer");
            jobCandidateDao.save(jobCandidate);
        } else {
            // false
        }
        return jobCandidateInterview;
    }
}
