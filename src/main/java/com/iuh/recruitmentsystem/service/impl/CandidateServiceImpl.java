package com.iuh.recruitmentsystem.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.dao.*;
import com.iuh.recruitmentsystem.model.dto.CandidateDto;
import com.iuh.recruitmentsystem.model.BlockCV;
import com.iuh.recruitmentsystem.model.dto.LogDetailDto;
import com.iuh.recruitmentsystem.model.dto.PersonDto;
import com.iuh.recruitmentsystem.model.dto.ResumeDto;
import com.iuh.recruitmentsystem.model.entity.*;
import com.iuh.recruitmentsystem.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.management.ObjectName;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

@Service(value = "candidateService")
public class CandidateServiceImpl implements CandidateService {

    @Autowired
    private CandidateDao candidateDao;

    @Autowired
    private PersonService personService;

    @Autowired
    private LogService logService;

    @Autowired
    private PersonDao personDao;

    @Autowired
    private CandidateTypeDao candidateTypeDao;

    @Autowired
    private CandidateDetailDao candidateDetailDao;

    @Autowired
    private UserService userService;

    @Autowired
    private JobCandidateService jobCandidateService;

    @Autowired
    private InterviewJobService interviewJobService;

    @Autowired
    private JobService jobService;


    @Override
    public Candidate findCandidateByID(long id) {

        Optional<Candidate> OptionalCandidate = candidateDao.findById(id);
        return OptionalCandidate.isPresent() ? OptionalCandidate.get() : null;
    }

    @Override
    @Transactional(rollbackFor = InValidRollbackException.class)
    public CandidateDto update(CandidateDto candidateDto, long id) throws InValidRollbackException {
        System.out.println(candidateDto.toString());
        Candidate candidate = findCandidateByID(id);
        if (candidate != null) {
            Person person = personService.update(candidateDto.getPerson(), candidate.getPerson().getId());
            candidate.setPerson(person);
            System.out.println(candidate.toString());
            candidateDao.save(candidate);
            logService.save(candidateDto.getLogDetail(), candidate, candidateDto.getUser_id());
        }
        return candidateDto;
    }


    @Override
    public Candidate save(CandidateDto candidateDto) {
        Candidate newCandidate = new Candidate();
        Person p = personService.save(candidateDto.getPerson());
        newCandidate.setPerson(p);
        return candidateDao.save(newCandidate);
    }

    @Override
    public List<Candidate> findAll() {
        List<Candidate> candidates = new ArrayList<>();
        candidateDao.findAll().iterator().forEachRemaining(candidates::add);
        return candidates;
    }

    @Override
    @Transactional(rollbackFor = InValidRollbackException.class)
    public Candidate applyCV(ResumeDto resumeDto) throws InValidRollbackException, JsonProcessingException {
        System.out.println(resumeDto);
        if (resumeDto.getResume().get("person") != null) {
            ObjectMapper mapper = new ObjectMapper();
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            Candidate candidate = savePersonResumeApply(mapper,resumeDto.getResume().get("person"));
            saveJobCandiate(resumeDto,candidate);
            if (resumeDto.getResume().get("education") !=null) {
                CandidateType candidateType = new CandidateType();
                candidateType.setName("EDUCATION");
                candidateType.setCandidate_type(candidate);
                candidateType = candidateTypeDao.save(candidateType);
        //        System.out.println("Header: " + candidateType.getName());
                Map<String, Object> map = mapper.convertValue(resumeDto.getResume().get("education"), Map.class);
                if(map.get("schools")!=null){
                    List<Object> schoolsArray = (ArrayList<Object>) map.get("schools");
                    for (Object school : schoolsArray) {
                        CandidateDetail candidateDetail = new CandidateDetail();
                        candidateDetail.setName("SCHOOLS");
                        String schoolJson = ow.writeValueAsString(school);
                        candidateDetail.setContent(schoolJson);
                        candidateDetail.setCandidateType(candidateType);
                   //     System.out.println("Name: "+ candidateDetail.getName());
                   //     System.out.println("Content: " + candidateDetail.getContent());
                        candidateDetailDao.save(candidateDetail);
                    }
                }
                if(map.get("examinations")!=null){
                    List<Object> examinationsArray = (ArrayList<Object>) map.get("examinations");
                    for (Object exam : examinationsArray) {
                        CandidateDetail candidateDetail = new CandidateDetail();
                        String examJson = ow.writeValueAsString(exam);
                        candidateDetail.setName("EXAMINATIONS");
                        candidateDetail.setContent(examJson);
                        candidateDetail.setCandidateType(candidateType);
                    //    System.out.println("Name1: "+ candidateDetail.getName());
                    //    System.out.println("Content: " + candidateDetail.getContent());
                        candidateDetailDao.save(candidateDetail);
                    }
                }
            }
            if (resumeDto.getResume().get("jobWork") !=null) {
                convertObjectToCandiDetail(ow,candidate,resumeDto.getResume().get("jobWork"),"JOBWORK","");
            }
            if (resumeDto.getResume().get("others") !=null) {
                convertObjectToCandiDetail(ow,candidate,resumeDto.getResume().get("others"),"OTHERS","");
            }
            if (resumeDto.getResume().get("projects") !=null) {
                convertObjectToCandiDetail(ow,candidate,resumeDto.getResume().get("projects"),"PROJECT","");
            }
            if (resumeDto.getResume().get("accomplishments") !=null) {
                convertObjectToCandiDetail(ow,candidate,resumeDto.getResume().get("accomplishments"),"ACCOMPLISHMENTS","");
            }
            if (resumeDto.getResume().get("skills") !=null) {
                convertObjectToCandiDetail(ow,candidate,resumeDto.getResume().get("skills"),"SKILLS","");
            }
        }else{
            System.out.println("Person Null");
        }
        return null;
    }

    @Override
    public List<Object[]> getResumeDetailByID(long candidateID) {
        System.out.println(candidateID);
        return candidateDao.getResumeDetailByID(candidateID);
    }

    public void saveJobCandiate(ResumeDto resumeDto, Candidate candidate){
        User user = userService.findById(resumeDto.getUserId());
        Job job = jobService.findById(resumeDto.getJobId());
        JobCandidate jobCandidate = new JobCandidate();
        jobCandidate.setCandidate(candidate);
        jobCandidate.setJob_candidate(job);
        jobCandidate.setPosition(resumeDto.getPosition());
        jobCandidate.setUrlCV(resumeDto.getUrlCV());
        jobCandidate.setStatus("waited");
        jobCandidate.setCurrentRound(0);
        jobCandidate = jobCandidateService.save(jobCandidate);

        InterviewerJob interviewerJob = new InterviewerJob();
        interviewerJob.setJobCandidate(jobCandidate);
        interviewerJob.setInterview_job(user);
        interviewerJob.setRule(3);
        interviewJobService.save(interviewerJob);
    }
    public Candidate savePersonResumeApply(ObjectMapper mapper, Object personObj){
        Candidate candidate = new Candidate();
        Map<String, Object> personMap = mapper.convertValue(personObj, Map.class);
        Person person = new Person();
        person.setEmail(personMap.get("email").toString());
        person.setAddress(personMap.get("address").toString());
        person.setBirthday(personMap.get("birthday").toString());
        person.setFirstName(personMap.get("firstName").toString());
        person.setLastName(personMap.get("lastName").toString());
        person.setObjective(personMap.get("objective").toString());
        person.setWebsite(personMap.get("website").toString());
        person.setJobTitle(personMap.get("jobTitle").toString());
        person.setPhoneNumber(personMap.get("phoneNumber").toString());
        person.setGender(personMap.get("gender").toString());
        candidate.setPerson(personDao.save(person));
        candidate = candidateDao.save(candidate);
        return  candidate;
    }
    public void convertObjectToCandiDetail(ObjectWriter ow,Candidate candidate,Object object, String header, String subheader) throws JsonProcessingException {
        CandidateType candidateType = new CandidateType();
        candidateType.setName(header);
        candidateType.setCandidate_type(candidate);
   //     System.out.println("Header: " + candidateType.getName());
        candidateType = candidateTypeDao.save(candidateType);
        List<Object> objectArray = (ArrayList<Object>) object;
        for (Object obj : objectArray) {
            CandidateDetail candidateDetail = new CandidateDetail();
            candidateDetail.setName(subheader);
            String objJson = ow.writeValueAsString(obj);
            candidateDetail.setContent(objJson);
            candidateDetail.setCandidateType(candidateType);
    //        System.out.println("Content: " + candidateDetail.getContent());
    //        System.out.println("Name: "+ candidateDetail.getName());
            candidateDetailDao.save(candidateDetail);
        }
    }
/*    public List<Object> converObjectArrayToMap(ObjectMapper mapper,Object object, String header){
        List<Object> objectArray = (ArrayList<Object>) object;
        Map<String,Object>  mapResult = new HashMap<>();
        for (Object obj : objectArray) {
            Map<String, String> map = mapper.convertValue(obj, Map.class);
    //        mapResult.put("name",map.get(header));
            mapResult.put("content", obj);
        }
        return mapResult;
    }*/

/*    @Override
    public Candidate extractCV(MultipartFile multipart) throws Exception {
        File file = convert(multipart);
        List<Map> s = new HtmlFile().convertPdfToHtml(file);
//        s.forEach(map -> {
//            System.out.println(map.get("line") + " - " +map.get("size"));
//        });

        executeExtractCV(clearLine(s));


*//*      String lines[] = handler.toString().split("\\r?\\n");
        String heuristic[]={};
        String blocks[];
        String filed[] ={"name", "address", "email", "phone", "mobile","telephone"};
        String blockheaderWords[]={"personal information","education",
                "research experience","award", "activity", "interests" ,"skill"};
        String personalInfo[] = {"name","gender","birthday","address","zip code","phone","mobile","email"};
        String educationInfo[] = {"gradiation school","degress","major","GPA","department"};
        System.out.println("File content : " + lines.length);

        for(int i=1; i<=lines.length; i++){
            // line.match(regex)
            // bỏ qua các dòng không có dữ liệu
            String line = lines[i];
            if(line.length()>2) {
              //  line = line.toLowerCase();
                StringTokenizer st = new StringTokenizer(line);
                System.out.println("Line "+i+":  "+line);
                while (st.hasMoreTokens()){
                    String token = st.nextToken();
                    String header = token.trim();
                    System.out.println("header "+ header);
                }
            }

        }
        System.out.println("File content : " + lines.length);*//*
        return null;
    }



    public File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public String trim(String data) {
        return data.replace("/\r?\n|\r|\t|\n/g", "").trim();
    }

    public String split(String data) {

        return "";
    }

    public String merge(String line1, String line2) {
        return line1 + line2;
    }

    private List<Map> clearLine(List<Map> s) {
        List<Map> lineRemove = new ArrayList<>();
        s.forEach(line -> {
            System.out.println(line);
            if (line.get("line").toString().trim().equals("")) {
                lineRemove.add(line);
            }
        });
        System.out.println("------------Line remove-------------");
        lineRemove.forEach(lnr -> {
            System.out.println(lnr);
            s.remove(lnr);
        });
        System.out.println("------------After remove-------------");
        s.forEach(line -> {
            System.out.println(line);

        });


        System.out.println("-------------------Done clear---------------");

        return s;
    }

    private BlockCV blockCV;

    private void executeExtractCV(List<Map> s) {

        List<BlockCV> listBlock = new ArrayList<>();
        final int[] flag = {-1, -1};
        blockCV = new BlockCV();

        s.forEach(map -> {

            try {

                int fontSize = Integer.parseInt(map.get("size").toString());

                if (fontSize >= flag[0]) {
                    // System.out.println(" >Bang ne:" + fontSize+"-"+flag[0]);

                    flag[0] = fontSize;
                    // System.out.println("> Bang ne:" + fontSize+"-"+flag[0]);

                    listBlock.add(blockCV);

                    blockCV = new BlockCV();

                    blockCV.addHeader(map.get("line").toString());
                }
                if (fontSize < flag[0]) {
                    //System.out.println("< ne, add content:" + fontSize+"-"+flag[0]);
                    blockCV.addContent(map.get("line").toString());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        });
        listBlock.remove(0);
        System.out.println("Block size: " + listBlock.size());


        for (BlockCV block : listBlock) {
            System.out.println("-----------------New block------------------");

            System.out.println("Header:");
            block.getHeader().forEach(head -> {
                System.out.println(head);
            });

            System.out.println("Content:");
            block.getContent().forEach(content -> {
                System.out.println(content);
            });

            System.out.println("-----------------End block------------------\n");

        }


    }*/
}
