package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.dao.ConfigDao;
import com.iuh.recruitmentsystem.model.entity.Config;
import com.iuh.recruitmentsystem.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "configService")
public class ConfigServiceImpl implements ConfigService {


    @Autowired
    private ConfigDao configDao;

    @Override
    public List<Config> findAll() {
        List<Config>list = new ArrayList<>();
        configDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public Config findById(long id) {
        return null;
    }
}
