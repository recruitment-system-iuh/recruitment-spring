package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.dao.PersonDao;
import com.iuh.recruitmentsystem.model.dto.PersonDto;
import com.iuh.recruitmentsystem.model.entity.Person;
import com.iuh.recruitmentsystem.service.PersonService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service(value = "personService")
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDao personDao;


    @Override
    public Person save(PersonDto person) {
        Person p = new Person();
        p.setId(person.getId());
        p.setAddress(person.getAddress());
        p.setBirthday(person.getBirthday());
        p.setFirstName(person.getFirstName());
        p.setLastName(person.getLastName());
        p.setPhoneNumber(person.getPhoneNumber());
        p.setEmail(person.getEmail());
        p.setGender(person.getGender());
        return personDao.save(p);
    }

    @Override
    public List<Person> findAll() {

        List<Person> list = new ArrayList<>();
        personDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    @Transactional
    public void delete(long id) {
        personDao.deleteById(id);
    }

    @Override
    public Person findById(long id) {

        Optional<Person> optionalPerson = Optional.ofNullable(personDao.findById(id));
        return optionalPerson.isPresent() ? optionalPerson.get() : null;
    }

    @Override
    public Person update(PersonDto PersonDto,long id) {
        Person person = findById(id);
        if(person != null) {
            BeanUtils.copyProperties(PersonDto, person,"id");
            person = personDao.save(person);
        }
        return person;
    }
}
