package com.iuh.recruitmentsystem.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.dao.ConfigDao;
import com.iuh.recruitmentsystem.dao.ConfigTypeDao;
import com.iuh.recruitmentsystem.dao.InterviewJobDao;
import com.iuh.recruitmentsystem.dao.JobDao;
import com.iuh.recruitmentsystem.model.dto.ConfigDto;
import com.iuh.recruitmentsystem.model.dto.JobDto;
import com.iuh.recruitmentsystem.model.entity.Config;
import com.iuh.recruitmentsystem.model.entity.ConfigType;
import com.iuh.recruitmentsystem.model.entity.Job;
import com.iuh.recruitmentsystem.model.entity.JobConfig;
import com.iuh.recruitmentsystem.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service(value = "jobService")
public class JobServiceImpl implements JobService {


    @Autowired
    private JobDao jobDao;

    @Autowired
    private ConfigTypeDao configTypeDao;

    @Autowired
    private ConfigDao configDao;

    @Override
    @Transactional(rollbackFor = InValidRollbackException.class)
    public Job save(JobDto jobdto) throws InValidRollbackException, JsonProcessingException {

        Job job = new Job();
        job.setJobName(jobdto.getJobName());
        job.setJobDescription(jobdto.getJobDescription());
        job.setPosition(jobdto.getPosition());
        job.setNumberRecruitment(jobdto.getNumberRecruitment());
        job.setRound(jobdto.getRound());
        job.setJobRequirement(jobdto.getJobRequirement());
        List<ConfigDto> configDtos = jobdto.getConfigs();
        List<JobConfig> jobConfigs = new ArrayList<>();
        for (ConfigDto configDto : configDtos) {
            System.out.println(configDto.getId());
            Config config = new Config();
            if (configDto.getId() == 0) {
                config.setName(configDto.getName());
                Optional<ConfigType> optionalConfigType = configTypeDao.findById(configDto.getConfigtype_id());
                config.setConfigType(optionalConfigType.isPresent() ? optionalConfigType.get() : null);
                config = configDao.save(config);
            }else {
                config.setId(configDto.getId());
                config.setName(configDto.getName());
            }
            JobConfig jobConfig = new JobConfig();
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            System.out.println(ow.writeValueAsString(configDto.getContent()));
            jobConfig.setContent(ow.writeValueAsString(configDto.getContent()));
            jobConfig.setJob_config(job);
            jobConfig.setConfig(config);
            jobConfigs.add(jobConfig);
        }
        // for all config
        job.setJobConfigs(jobConfigs);
        return jobDao.save(job);
    }

    @Override
    public List<Job> findAll() {

        List<Job> list = new ArrayList<>();
        jobDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }


    @Override
    public void delete(long id) {

    }

    @Override
    public Job findById(long id) {

        Optional<Job> optionalJob = jobDao.findById(id);
        return optionalJob.isPresent() ? optionalJob.get(): null;
    }

    @Override
    public JobDto update(JobDto jobDto) {
        return null;
    }
}
