package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.dao.InterviewJobDao;
import com.iuh.recruitmentsystem.model.entity.InterviewerJob;
import com.iuh.recruitmentsystem.model.hellper.CustormInterviewJobDto;
import com.iuh.recruitmentsystem.model.dto.InterviewerJobDto;
import com.iuh.recruitmentsystem.service.InterviewJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "interviewJobService")
public class InterviewJobServiceImpl implements InterviewJobService {

    @Autowired
   private InterviewJobDao interviewJobDao;

    @Override
    public List<InterviewerJobDto> listJobByUserID(long userid, long jobid) {
        List<InterviewerJobDto> list = new ArrayList<>();
        List<CustormInterviewJobDto> listCustorm = interviewJobDao.findAllJobByUserID(userid,jobid);
        listCustorm.forEach(custormInterviewJobDto ->{
            InterviewerJobDto interviewerJobDto = new InterviewerJobDto();
            interviewerJobDto.setJob_id(custormInterviewJobDto.getID());
            interviewerJobDto.setJobname(custormInterviewJobDto.getJOBNAME());
            interviewerJobDto.setPosition(custormInterviewJobDto.getPOSITION());
            interviewerJobDto.setJobRound(custormInterviewJobDto.getJOBROUND());
            interviewerJobDto.setName(custormInterviewJobDto.getFIRSTNAME() + " " + custormInterviewJobDto.getLASTNAME());
            interviewerJobDto.setEmail(custormInterviewJobDto.getEMAIL());
            interviewerJobDto.setJobcanID(custormInterviewJobDto.getJOBCAND_ID());
            interviewerJobDto.setCreatedAt(custormInterviewJobDto.getCREATEDATETIME());
            interviewerJobDto.setJvRound(custormInterviewJobDto.getJVROUND());
            interviewerJobDto.setJci_id(custormInterviewJobDto.getJCI_ID());
            list.add(interviewerJobDto);
        });
        return list;
    }

    @Override
    public InterviewerJob save(InterviewerJob interviewerJob) {
        return interviewJobDao.save(interviewerJob);
    }
}
