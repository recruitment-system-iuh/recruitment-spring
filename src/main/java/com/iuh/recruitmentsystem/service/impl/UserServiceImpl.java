package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.dao.UserDao;
import com.iuh.recruitmentsystem.model.dto.PersonDto;
import com.iuh.recruitmentsystem.model.entity.Person;
import com.iuh.recruitmentsystem.model.dto.UserDto;
import com.iuh.recruitmentsystem.model.entity.User;
import com.iuh.recruitmentsystem.service.PersonService;
import com.iuh.recruitmentsystem.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private PersonService personService;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("user name " + username);
		User user = userDao.findByUsername(username);
		System.out.println("user dau" + user.toString());
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(
				user.getUsername(), user.getPassword(), getAuthority());
	}
	@Override
	public User loadUserIDByUsername(String username) throws UsernameNotFoundException {
		User user = userDao.findByUsername(username);
		return user;
	}

	@Override
	public List<User> listUserByRule(int rule) {
		List<User> list = new ArrayList<>();
		userDao.findAllByRule(rule).iterator().forEachRemaining(list::add);
		return list;
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<User> findAll() {
		List<User> list = new ArrayList<>();
		userDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	@Transactional
	public void delete(long id) {
		userDao.deleteById(id);
	}

	@Override
	public User findOne(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public User findById(long id) {
		Optional<User> optionalUser = userDao.findById(id);
		return optionalUser.isPresent() ? optionalUser.get() : null;
	}

    @Override
    public UserDto update(UserDto userDto,long id) {
        User user = findById(id);
        System.out.println(user.getRule());
        if(user != null) {
            BeanUtils.copyProperties(userDto, user, "password","id");
			Person person = personService.update(userDto.getPerson(),user.getPerson().getId());
			user.setPerson(person);
			userDao.save(user);
        }
        return userDto;
    }

    @Override
    public User save(UserDto user) {
	    User newUser = new User();
	    newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
	    newUser.setRule(user.getRule());
	    Person p = personService.save(user.getPerson());
	    newUser.setPerson(p);
	    System.out.println( user.getUsername() + "---"+ bcryptEncoder.encode(user.getPassword()));
        return userDao.save(newUser);
    }
}
