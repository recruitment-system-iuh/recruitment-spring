package com.iuh.recruitmentsystem.service.impl;

import com.iuh.recruitmentsystem.dao.JobConfigDao;
import com.iuh.recruitmentsystem.model.entity.Job;
import com.iuh.recruitmentsystem.model.entity.JobConfig;
import com.iuh.recruitmentsystem.service.JobConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "jobConfigService")
public class JobConfigServiceImpl implements JobConfigService {


    @Autowired
    private JobConfigDao jobConfigDao;

    @Override
    public List<Object[]> findAllByJobID(long job_id) {
        return jobConfigDao.findAllByJobId(job_id);
    }

    @Override
    public JobConfig findById(long id) {
        return null;
    }
}
