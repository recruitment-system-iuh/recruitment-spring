package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.config.InValidRollbackException;
import com.iuh.recruitmentsystem.model.dto.LogDetailDto;
import com.iuh.recruitmentsystem.model.entity.Candidate;
import com.iuh.recruitmentsystem.model.entity.Log;

import java.util.List;

public interface LogService {

    Log save(List<LogDetailDto> logDetails, Candidate candidate, long user_id) throws InValidRollbackException;

    List<Log> listLogByTimeline(LogDetailDto logDto);

    List<Log> findAllByCandidateID();
}
