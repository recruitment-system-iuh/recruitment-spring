package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.model.dto.JobCandidateDto;
import com.iuh.recruitmentsystem.model.dto.InterviewCandiStatusDto;
import com.iuh.recruitmentsystem.model.entity.JobCandidate;
import com.iuh.recruitmentsystem.model.hellper.CustormDataFeedbackDto;
import com.iuh.recruitmentsystem.model.hellper.CustormResumeJobCandiDto;

import java.util.List;

public interface JobCandidateService {

    List<JobCandidateDto> listJobCandidateByStatus(String status);

    JobCandidate updateJobCandidateWithStatusViewed(long jobcandidateId);

    JobCandidateDto updateIdInterviewRoundJobCandidate(long jobcandidateId, long userid);


    JobCandidate updateCurrentRoundJobCandidate(JobCandidate jobCandidate, long currentRound);

    List<JobCandidate> listJobCandidateByStatusAndJobId(String status,long id);

    JobCandidate findCandidateByID(long id);

    List<InterviewCandiStatusDto> listInterviewCandiStatusByJobID(long jobid);

    List<Integer> reportJobCandiByOneMonth();

    CustormDataFeedbackDto getFeedbackData(long jobcandID);

    JobCandidate save(JobCandidate jobCandidate);

    CustormResumeJobCandiDto getReviewData( long jobID, long candId);
}

