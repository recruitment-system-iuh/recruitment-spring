package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.model.dto.JobDto;
import com.iuh.recruitmentsystem.model.entity.Config;
import com.iuh.recruitmentsystem.model.entity.Job;

import java.util.List;

public interface ConfigService {

 //   Config save(JobDto job);

    List<Config> findAll();

    void delete(long id);

    Config findById(long id);

}
