package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.model.dto.InterviewerJobDto;
import com.iuh.recruitmentsystem.model.entity.InterviewerJob;

import java.util.List;

public interface InterviewJobService {

    List<InterviewerJobDto> listJobByUserID(long userid, long jobid);
    InterviewerJob save(InterviewerJob interviewerJob);
}
