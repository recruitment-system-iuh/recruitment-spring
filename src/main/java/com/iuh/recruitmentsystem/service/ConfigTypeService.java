package com.iuh.recruitmentsystem.service;

import com.iuh.recruitmentsystem.model.hellper.CustormConfigTypeDto;
import com.iuh.recruitmentsystem.model.entity.ConfigType;

import java.util.List;

public interface ConfigTypeService {

   // ConfigType save(JobDto job);

    List<ConfigType> findAll();

    List<CustormConfigTypeDto> listModelById(long id);
}
