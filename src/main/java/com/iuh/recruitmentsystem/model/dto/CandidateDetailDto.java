package com.iuh.recruitmentsystem.model.dto;

import java.util.List;

public class CandidateDetailDto {

    private Long id;

    private String name;


    private String content;


    private CandidateTypeDto candidateType;

    private List<JobCandidateDetailDto> jobCandidateDetails;

    public CandidateDetailDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public CandidateTypeDto getCandidateType() {
        return candidateType;
    }

    public void setCandidateType(CandidateTypeDto candidateType) {
        this.candidateType = candidateType;
    }

    public List<JobCandidateDetailDto> getJobCandidateDetails() {
        return jobCandidateDetails;
    }

    public void setJobCandidateDetails(List<JobCandidateDetailDto> jobCandidateDetails) {
        this.jobCandidateDetails = jobCandidateDetails;
    }
}
