package com.iuh.recruitmentsystem.model.dto;

public class InterviewerJobDto {


    private long job_id;
    private String jobname;
    private String position;
    private int jobRound;
    private String jobDescription;
    private String name;
    private String email;
    private long jobcanID;
    private String createdAt;
    private int jvRound;
    private int jci_id;
    public InterviewerJobDto() {
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public long getJob_id() {
        return job_id;
    }

    public void setJob_id(long job_id) {
        this.job_id = job_id;
    }

    public String getJobname() {
        return jobname;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getJobcanID() {
        return jobcanID;
    }

    public void setJobcanID(long jobcanID) {
        this.jobcanID = jobcanID;
    }

    public int getJobRound() {
        return jobRound;
    }

    public void setJobRound(int jobRound) {
        this.jobRound = jobRound;
    }

    public int getJvRound() {
        return jvRound;
    }

    public void setJvRound(int jvRound) {
        this.jvRound = jvRound;
    }

    public int getJci_id() {
        return jci_id;
    }

    public void setJci_id(int jci_id) {
        this.jci_id = jci_id;
    }
}
