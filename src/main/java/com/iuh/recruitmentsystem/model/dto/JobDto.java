package com.iuh.recruitmentsystem.model.dto;

import java.util.List;

public class JobDto {

    private long id;

    private String jobDescription;

    private String jobRequirement;

    private String jobName;

    private String position;

    private int round;

    private int numberRecruitment;

    private List<ConfigDto> configs;

    public JobDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public int getNumberRecruitment() {
        return numberRecruitment;
    }

    public void setNumberRecruitment(int numberRecruitment) {
        this.numberRecruitment = numberRecruitment;
    }

    public List<ConfigDto> getConfigs() {
        return configs;
    }

    public void setConfigs(List<ConfigDto> configs) {
        this.configs = configs;
    }

    public String getJobRequirement() {
        return jobRequirement;
    }

    public void setJobRequirement(String jobRequirement) {
        this.jobRequirement = jobRequirement;
    }

    @Override
    public String toString() {
        return "JobDto{" +
                "id=" + id +
                ", jobDescription='" + jobDescription + '\'' +
                ", jobRequirement='" + jobRequirement + '\'' +
                ", jobName='" + jobName + '\'' +
                ", position='" + position + '\'' +
                ", round=" + round +
                ", numberRecruitment=" + numberRecruitment +
                ", configs=" + configs +
                '}';
    }
}
