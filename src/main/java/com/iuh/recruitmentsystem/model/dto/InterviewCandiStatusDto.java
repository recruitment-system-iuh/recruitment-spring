package com.iuh.recruitmentsystem.model.dto;

public class InterviewCandiStatusDto {

    private long id;
    private long jobcanID;
    private String jobname;
    private String position;
    private int jobRound;
    private String numberRecruitment;
    private String candidateName;
    private String email;
    private String phoneNumber;
    private String createdDate;
    private int interviewRound;
    private String statusInterview;
    private String jobcandiStatus;

    public InterviewCandiStatusDto() {
    }

    public long getJobcanID() {
        return jobcanID;
    }

    public void setJobcanID(long jobcanID) {
        this.jobcanID = jobcanID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJobname() {
        return jobname;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getJobRound() {
        return jobRound;
    }

    public void setJobRound(int jobRound) {
        this.jobRound = jobRound;
    }

    public String getNumberRecruitment() {
        return numberRecruitment;
    }

    public void setNumberRecruitment(String numberRecruitment) {
        this.numberRecruitment = numberRecruitment;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    public String getStatusInterview() {
        return statusInterview;
    }

    public void setStatusInterview(String statusInterview) {
        this.statusInterview = statusInterview;
    }

    public String getJobcandiStatus() {
        return jobcandiStatus;
    }

    public void setJobcandiStatus(String jobcandiStatus) {
        this.jobcandiStatus = jobcandiStatus;
    }

    public int getInterviewRound() {
        return interviewRound;
    }

    public void setInterviewRound(int interviewRound) {
        this.interviewRound = interviewRound;
    }
}
