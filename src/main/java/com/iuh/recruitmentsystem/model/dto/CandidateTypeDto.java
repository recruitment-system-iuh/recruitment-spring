package com.iuh.recruitmentsystem.model.dto;

import java.util.List;

public class CandidateTypeDto {

    private Long id;


    private String name;

    private CandidateDto candidate_type;

    private List<CandidateDetailDto> candidateDetails;

    public CandidateTypeDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CandidateDto getCandidate_type() {
        return candidate_type;
    }

    public void setCandidate_type(CandidateDto candidate_type) {
        this.candidate_type = candidate_type;
    }

    public List<CandidateDetailDto> getCandidateDetails() {
        return candidateDetails;
    }

    public void setCandidateDetails(List<CandidateDetailDto> candidateDetails) {
        this.candidateDetails = candidateDetails;
    }
}
