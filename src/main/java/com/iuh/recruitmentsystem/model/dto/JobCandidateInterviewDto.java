package com.iuh.recruitmentsystem.model.dto;

import java.util.Date;

public class JobCandidateInterviewDto {

    private Long id;

    private int round;

    private String status;

    private String feedback;

    private Date dateTime;

    private String address;

    private String questionInterview;

    private String answerInterview;


    public JobCandidateInterviewDto() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getQuestionInterview() {
        return questionInterview;
    }

    public void setQuestionInterview(String questionInterview) {
        this.questionInterview = questionInterview;
    }

    public String getAnswerInterview() {
        return answerInterview;
    }

    public void setAnswerInterview(String answerInterview) {
        this.answerInterview = answerInterview;
    }
}
