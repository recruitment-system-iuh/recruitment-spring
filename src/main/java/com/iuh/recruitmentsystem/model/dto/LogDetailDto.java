package com.iuh.recruitmentsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.iuh.recruitmentsystem.model.entity.Candidate;
import com.iuh.recruitmentsystem.model.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;


public class LogDetailDto {

    private String changeValue;
    private String toValue;
    private LocalDateTime createDateTime;
    private String updateField;

    public LogDetailDto() {
    }

    public String getChangeValue() {
        return changeValue;
    }

    public void setChangeValue(String changeValue) {
        this.changeValue = changeValue;
    }

    public String getToValue() {
        return toValue;
    }

    public void setToValue(String toValue) {
        this.toValue = toValue;
    }

    public String getUpdateField() {
        return updateField;
    }

    public void setUpdateField(String updateField) {
        this.updateField = updateField;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    @Override
    public String toString() {
        return "LogDto{" +
                "changeValue='" + changeValue + '\'' +
                ", toValue='" + toValue + '\'' +
                ", createDateTime=" + createDateTime +
                ", updateField='" + updateField + '\'' +
                '}';
    }
}
