package com.iuh.recruitmentsystem.model.dto;

import java.util.Arrays;

public class ScheduleDto {

    private String time;
    private String address;
    private int[] supporters;
    private int interviewer;
    private String[] jobcandIDs;

    public ScheduleDto() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setJobcandIDs(String[] jobcandIDs) {
        this.jobcandIDs = jobcandIDs;
    }

    public int getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(int interviewer) {
        this.interviewer = interviewer;
    }

    public int[] getSupporters() {
        return supporters;
    }

    public void setSupporters(int[] supporters) {
        this.supporters = supporters;
    }

    @Override
    public String toString() {
        return "ScheduleDto{" +
                "time='" + time + '\'' +
                ", address='" + address + '\'' +
                ", supporters=" + Arrays.toString(supporters) +
                ", interviewer=" + interviewer +
                ", jobcandIDs=" + Arrays.toString(jobcandIDs) +
                '}';
    }

    public String[] getJobcandIDs() {
        return jobcandIDs;
    }
}
