package com.iuh.recruitmentsystem.model.dto;

public class JobCandidateDetailDto {


    private Long id;

    private int matchingPoint;

    private JobConfigDto jobConfig;


    private JobCandidateDto jobCandidate;

    private CandidateDetailDto candidateDetail;

    public JobCandidateDetailDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMatchingPoint() {
        return matchingPoint;
    }

    public void setMatchingPoint(int matchingPoint) {
        this.matchingPoint = matchingPoint;
    }

    public JobConfigDto getJobConfig() {
        return jobConfig;
    }

    public void setJobConfig(JobConfigDto jobConfig) {
        this.jobConfig = jobConfig;
    }

    public JobCandidateDto getJobCandidate() {
        return jobCandidate;
    }

    public void setJobCandidate(JobCandidateDto jobCandidate) {
        this.jobCandidate = jobCandidate;
    }

    public CandidateDetailDto getCandidateDetail() {
        return candidateDetail;
    }

    public void setCandidateDetail(CandidateDetailDto candidateDetail) {
        this.candidateDetail = candidateDetail;
    }
}
