package com.iuh.recruitmentsystem.model.dto;

import java.util.List;

public class JobConfigDto {

    private Long id;


    private ConfigDto config;

    private JobDto job_config;

    private List<JobCandidateDetailDto> jobCandidateDetails;

    public JobConfigDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConfigDto getConfig() {
        return config;
    }

    public void setConfig(ConfigDto config) {
        this.config = config;
    }

    public JobDto getJob_config() {
        return job_config;
    }

    public void setJob_config(JobDto job_config) {
        this.job_config = job_config;
    }

    public List<JobCandidateDetailDto> getJobCandidateDetails() {
        return jobCandidateDetails;
    }

    public void setJobCandidateDetails(List<JobCandidateDetailDto> jobCandidateDetails) {
        this.jobCandidateDetails = jobCandidateDetails;
    }
}
