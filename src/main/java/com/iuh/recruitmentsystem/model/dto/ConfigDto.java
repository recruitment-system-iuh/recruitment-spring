package com.iuh.recruitmentsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.iuh.recruitmentsystem.model.entity.ConfigType;

import java.util.List;
import java.util.Map;

public class ConfigDto {

    private long id;
    private String name;
    private Map<String,Object> content;
    private long configtype_id;
//    private ConfigTypeDto configType;
//    private List<JobConfigDto> jobConfigs;

    public ConfigDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public long getConfigtype_id() {
        return configtype_id;
    }

    public void setConfigtype_id(long configtype_id) {
        this.configtype_id = configtype_id;
    }

    public Map<String, Object> getContent() {
        return content;
    }

    public void setContent(Map<String, Object> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ConfigDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content=" + content +
                ", configtype_id=" + configtype_id +
                '}';
    }
}
