package com.iuh.recruitmentsystem.model.dto;

import java.util.Map;

public class ResumeDto {

    private long userId;
    private long jobId;
    private String position;
    private String urlCV;
    private Map<String, Object> resume;

    public ResumeDto() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Map<String, Object> getResume() {
        return resume;
    }

    public void setResume(Map<String, Object> resume) {
        this.resume = resume;
    }

    public String getUrlCV() {
        return urlCV;
    }

    public void setUrlCV(String urlCV) {
        this.urlCV = urlCV;
    }

    @Override
    public String toString() {
        return "ResumeDto{" +
                "userId=" + userId +
                ", jobId=" + jobId +
                ", position='" + position + '\'' +
                ", urlCV='" + urlCV + '\'' +
                ", resume=" + resume +
                '}';
    }
}
