package com.iuh.recruitmentsystem.model.dto;

public class ScheduleDetailDto {

    private long jobcanId;
    private int round;
    private String status;
    private String dateTime;
    private String feedback;
    private String address;
    private String userFeedbackName;

    public ScheduleDetailDto() {
    }

    public long getJobcanId() {
        return jobcanId;
    }

    public void setJobcanId(long jobcanId) {
        this.jobcanId = jobcanId;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserFeedbackName() {
        return userFeedbackName;
    }

    public void setUserFeedbackName(String userFeedbackName) {
        this.userFeedbackName = userFeedbackName;
    }
}
