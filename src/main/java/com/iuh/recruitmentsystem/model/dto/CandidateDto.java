package com.iuh.recruitmentsystem.model.dto;

import java.util.List;

public class CandidateDto {

    private Long user_id;
    private PersonDto person;
    private List<LogDetailDto> logDetail;
    public CandidateDto() {
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public PersonDto getPerson() {
        return person;
    }

    public void setPerson(PersonDto person) {
        this.person = person;
    }

    public List<LogDetailDto> getLogDetail() {
        return logDetail;
    }

    public void setLogDetail(List<LogDetailDto> logDetail) {
        this.logDetail = logDetail;
    }

    @Override
    public String toString() {
        return "CandidateDto{" +
                "user_id=" + user_id +
                ", person=" + person +
                ", logDetail=" + logDetail +
                '}';
    }
}
