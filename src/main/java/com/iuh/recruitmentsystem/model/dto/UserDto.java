package com.iuh.recruitmentsystem.model.dto;

public class UserDto {

    private long id;
    private String username;
    private String password;
    private int rule;
    //    private String firstName;
//    private String lastName;
//    private String birthday;
//    private String address;
//    private String phoneNumber;
    private PersonDto person;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRule() {
        return rule;
    }

    public void setRule(int rule) {
        this.rule = rule;
    }

    public PersonDto getPerson() {
        return person;
    }

    public void setPerson(PersonDto person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", rule=" + rule +
                ", person=" + person +
                '}';
    }
}