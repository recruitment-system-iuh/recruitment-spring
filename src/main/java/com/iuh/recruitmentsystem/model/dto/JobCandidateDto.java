package com.iuh.recruitmentsystem.model.dto;

import java.time.LocalDateTime;

public class JobCandidateDto {

    private long id;
    private long currentRound;
    private long idInterviewRound;
    private String status;
    private String statusInterview;
    private LocalDateTime createDateTime;
    private LocalDateTime updateDateTime;
    private String urlCV;
    private long candidateID;
    private long jobID;
    private String position;
    private String candidateName;
    private String jobName;

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public JobCandidateDto() {

    }

    public long getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(long currentRound) {
        this.currentRound = currentRound;
    }

    public long getIdInterviewRound() {
        return idInterviewRound;
    }

    public void setIdInterviewRound(long idInterviewRound) {
        this.idInterviewRound = idInterviewRound;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusInterview() {
        return statusInterview;
    }

    public void setStatusInterview(String statusInterview) {
        this.statusInterview = statusInterview;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public String getUrlCV() {
        return urlCV;
    }

    public void setUrlCV(String urlCV) {
        this.urlCV = urlCV;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCandidateID() {
        return candidateID;
    }

    public void setCandidateID(long candidateID) {
        this.candidateID = candidateID;
    }

    public long getJobID() {
        return jobID;
    }

    public void setJobID(long jobID) {
        this.jobID = jobID;
    }
}
