package com.iuh.recruitmentsystem.model.hellper;

import java.util.List;

public class CustormResumeJobCandiDto {

    private String jobName;
    private String jobDescription;;
    private String position;
    private String jobRequirement;
    private List<CustormResumeDetail> resumeDetais;
    private List<CustormJobConfig> jobConfigs;

    public CustormResumeJobCandiDto() {
    }
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public List<CustormResumeDetail> getResumeDetais() {
        return resumeDetais;
    }

    public void setResumeDetais(List<CustormResumeDetail> resumeDetais) {
        this.resumeDetais = resumeDetais;
    }

    public List<CustormJobConfig> getJobConfigs() {
        return jobConfigs;
    }

    public void setJobConfigs(List<CustormJobConfig> jobConfigs) {
        this.jobConfigs = jobConfigs;
    }

    public String getJobRequirement() {
        return jobRequirement;
    }

    public void setJobRequirement(String jobRequirement) {
        this.jobRequirement = jobRequirement;
    }
}

