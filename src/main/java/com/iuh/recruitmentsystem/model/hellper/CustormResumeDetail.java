package com.iuh.recruitmentsystem.model.hellper;

public class CustormResumeDetail {

    private String header;
    private String subHeader;
    private String content;

    public CustormResumeDetail() {
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
