package com.iuh.recruitmentsystem.model.hellper;

import javax.persistence.Lob;

public interface CustormInterviewJobDto {

    long getID();
    String getJOBNAME();
    String getPOSITION();
    int getJOBROUND();
    String getFIRSTNAME();
    String getLASTNAME();
    String getEMAIL();
    long getJOBCAND_ID();
    String getCREATEDATETIME();
    int getJVROUND();
    int getJCI_ID();

}
