package com.iuh.recruitmentsystem.model.hellper;

public interface CustormScheduleDetailDto {

    long getId();
    int getROUND();
    String getSTATUS();
    String getDATETIME();
    String getFEEDBACK();
    String getADDRESS();
    String getFIRSTNAME();
    String getLASTNAME();
}
