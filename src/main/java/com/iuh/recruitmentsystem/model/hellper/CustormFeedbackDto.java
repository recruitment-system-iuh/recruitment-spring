package com.iuh.recruitmentsystem.model.hellper;

public class CustormFeedbackDto {

    private long status;
    private String feedback;
    private String answerInterview;
    public CustormFeedbackDto() {

    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getAnswerInterview() {
        return answerInterview;
    }

    public void setAnswerInterview(String answerInterview) {
        this.answerInterview = answerInterview;
    }
}

