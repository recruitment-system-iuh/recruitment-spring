package com.iuh.recruitmentsystem.model.hellper;

public interface CustormConfigTypeDto {

    long getId();
    String getName();
    String getNameModel();
    String getProperty1();
    String getType1();
    String getProperty2();
    String getType2();
}
