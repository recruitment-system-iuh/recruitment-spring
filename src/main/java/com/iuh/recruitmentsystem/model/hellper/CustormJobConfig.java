package com.iuh.recruitmentsystem.model.hellper;

public class CustormJobConfig{

    private String content;
    private long configTypeID;
    private String configName;
    private String configTypeName;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getConfigTypeID() {
        return configTypeID;
    }

    public void setConfigTypeID(long configTypeID) {
        this.configTypeID = configTypeID;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigTypeName() {
        return configTypeName;
    }

    public void setConfigTypeName(String configTypeName) {
        this.configTypeName = configTypeName;
    }
}
