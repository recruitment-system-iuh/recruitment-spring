package com.iuh.recruitmentsystem.model.hellper;

public interface CustormReportJobCandidate {

    int getTotal();
    int getWaited();
    int getViewed();
    int getInterview();
    int getJobnew();
    int getOffer();
}
