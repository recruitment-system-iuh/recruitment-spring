package com.iuh.recruitmentsystem.model.hellper;


public class CustormDataFeedbackDto {

    private String jobName;
    private String p_Recruitment;
    private String name;
    private String email;
    private String p_apply;
    private String questionInterview;
    private String urlCV;
    private int round;
    private String address;
    private String date_Interview;

    public CustormDataFeedbackDto() {
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getP_Recruitment() {
        return p_Recruitment;
    }

    public void setP_Recruitment(String p_Recruitment) {
        this.p_Recruitment = p_Recruitment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getP_apply() {
        return p_apply;
    }

    public void setP_apply(String p_apply) {
        this.p_apply = p_apply;
    }

    public String getQuestionInterview() {
        return questionInterview;
    }

    public void setQuestionInterview(String questionInterview) {
        this.questionInterview = questionInterview;
    }

    public String getUrlCV() {
        return urlCV;
    }

    public void setUrlCV(String urlCV) {
        this.urlCV = urlCV;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate_Interview() {
        return date_Interview;
    }

    public void setDate_Interview(String date_Interview) {
        this.date_Interview = date_Interview;
    }
}
