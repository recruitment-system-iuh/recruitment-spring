package com.iuh.recruitmentsystem.model.hellper;

public interface CustormJobCandidateScheduleByID {

    long getID();
    long getJOBCANDID();
    String getJOBNAME();
    String getPOSITION();
    int getJOBROUND();
    String getNUMBERRECRUITMENT();
    String getFIRSTNAME();
    String getLASTNAME();
    String getEMAIL();
    String getPHONENUMBER();
    String getCREATEDATETIME();
    int getINTERVIEWROUND();
    String getSTATUS();
    String getJOBCANDSTATUS();
}
