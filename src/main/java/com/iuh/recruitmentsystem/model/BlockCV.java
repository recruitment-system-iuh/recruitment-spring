package com.iuh.recruitmentsystem.model;

import java.util.ArrayList;
import java.util.List;

public class BlockCV {
    private List<String> header ;
    private List<String> content;

    @Override
    public String toString() {
        return "BlockCV{" +
                "header=" + header +
                ", content=" + content +
                '}';
    }

    public List<String> getHeader() {
        return header;
    }

    public void setHeader(List<String> header) {
        this.header = header;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }

    public BlockCV(List<String> header) {
        this.header = header;
    }

    public void addHeader(String newHead){
        header.add(newHead);
    }
    public void addContent(String newContent){
        content.add(newContent);
    }

    public BlockCV() {
        this.header =new ArrayList<>();
        this.content=new ArrayList<>();

    }
}
