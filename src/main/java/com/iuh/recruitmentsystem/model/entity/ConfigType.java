package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ConfigTypes")
@ApiModel(description = "All details about the ConfigType")
public class ConfigType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated ConfigType ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "ConfigType Name")
    private String name;

    @Column
    @Lob
    @ApiModelProperty(notes = "Template generate form dynamic")
    private String content;


    @JsonManagedReference
    @OneToMany(mappedBy = "configType", cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List Config")
    private List<Config> configs;



    public ConfigType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Config> getConfigs() {
        return configs;
    }

    public void setConfigs(List<Config> configs) {
        this.configs = configs;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
