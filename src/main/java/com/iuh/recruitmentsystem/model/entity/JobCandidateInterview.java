package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "JobCandidateInterviews")
@ApiModel(description = "All details about the JobCandidateInterview")
public class JobCandidateInterview {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated JobCandidateInterview ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "Round Interview")
    private int round;

    @Column
    @ApiModelProperty(notes = "Status Interview Round")
    private String status;

    @Column
    @ApiModelProperty(notes = "Feedback")
    private String feedback;

    @Column
    @ApiModelProperty(notes = "DateTime")
    private Date dateTime;

    @Column
    @ApiModelProperty(notes = "address")
    private String address;

    @Lob
    @Column
    @ApiModelProperty(notes = "Interview Question")
    private String questionInterview;

    @Lob
    @Column
    @ApiModelProperty(notes = "Interview Answer")
    private String answerInterview;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "JobCand_ID")
    @NotNull
    @ApiModelProperty(notes = "")
    private JobCandidate JobCandidate;

    public JobCandidateInterview() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public com.iuh.recruitmentsystem.model.entity.JobCandidate getJobCandidate() {
        return JobCandidate;
    }

    public void setJobCandidate(com.iuh.recruitmentsystem.model.entity.JobCandidate jobCandidate) {
        JobCandidate = jobCandidate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getQuestionInterview() {
        return questionInterview;
    }

    public void setQuestionInterview(String questionInterview) {
        this.questionInterview = questionInterview;
    }

    public String getAnswerInterview() {
        return answerInterview;
    }

    public void setAnswerInterview(String answerInterview) {
        this.answerInterview = answerInterview;
    }

    @Override
    public String toString() {
        return "JobCandidateInterview{" +
                "id=" + id +
                ", round=" + round +
                ", status='" + status + '\'' +
                ", feedback='" + feedback + '\'' +
                ", dateTime=" + dateTime +
                ", address='" + address + '\'' +
                ", JobCandidate=" + JobCandidate +
                '}';
    }
}
