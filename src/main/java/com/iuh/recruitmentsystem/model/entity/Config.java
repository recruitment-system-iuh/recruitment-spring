package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "Configs")
@ApiModel(description = "All details about the Config")
public class Config {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated Config ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "Config name: Java,C#")
    private String name;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "ConfigType_ID")
    @NotNull
    @ApiModelProperty(notes = "Configtype: Technical, Bussiness")
    private ConfigType configType;

    @JsonIgnore
    @OneToMany(mappedBy = "config",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List JobConfig")
    private List<JobConfig> jobConfigs;

    public Config() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ConfigType getConfigType() {
        return configType;
    }

    public void setConfigType(ConfigType configType) {
        this.configType = configType;
    }

    public List<JobConfig> getJobConfigs() {
        return jobConfigs;
    }

    public void setJobConfigs(List<JobConfig> jobConfigs) {
        this.jobConfigs = jobConfigs;
    }
}
