package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "LogDetails")
@ApiModel(description = "All details about the log")
public class LogDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated Log ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "Value old")
    private String changeValue;

    @ApiModelProperty(notes = "Value New")
    @Column
    private String toValue;

    @Column
    @ApiModelProperty(notes = "Column Update")
    private String updateField;

   // @JsonBackReference
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "log_id")
    @NotNull
    @ApiModelProperty(notes = "")
    private Log logs;

    public LogDetail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getChangeValue() {
        return changeValue;
    }

    public void setChangeValue(String changeValue) {
        this.changeValue = changeValue;
    }

    public String getToValue() {
        return toValue;
    }

    public void setToValue(String toValue) {
        this.toValue = toValue;
    }

    public String getUpdateField() {
        return updateField;
    }

    public void setUpdateField(String updateField) {
        this.updateField = updateField;
    }

    public Log getLogs() {
        return logs;
    }

    public void setLogs(Log logs) {
        this.logs = logs;
    }

    @Override
    public String toString() {
        return "LogDetail{" +
                "id=" + id +
                ", changeValue='" + changeValue + '\'' +
                ", toValue='" + toValue + '\'' +
                ", updateField='" + updateField + '\'' +
                ", logs=" + logs +
                '}';
    }
}
