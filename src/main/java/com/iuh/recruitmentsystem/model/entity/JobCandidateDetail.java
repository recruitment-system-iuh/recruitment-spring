package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "JobCandidateDetails")
@ApiModel(description = "All details about the JobCandidateDetail")
public class JobCandidateDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated JobCandidateDetail ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "Matching Point")
    private int matchingPoint;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "JobConfig_ID")
    @NotNull
    @ApiModelProperty(notes = "")
    private JobConfig jobConfig;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "JobCandidate_ID")
    @NotNull
    @ApiModelProperty(notes = "")
    private JobCandidate jobCandidate;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "CandidateDetail_ID")
    @NotNull
    @ApiModelProperty(notes = "")
    private CandidateDetail candidateDetail;


    public JobCandidateDetail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMatchingPoint() {
        return matchingPoint;
    }

    public void setMatchingPoint(int matchingPoint) {
        this.matchingPoint = matchingPoint;
    }

    public JobConfig getJobConfig() {
        return jobConfig;
    }

    public void setJobConfig(JobConfig jobConfig) {
        this.jobConfig = jobConfig;
    }

    public JobCandidate getJobCandidate() {
        return jobCandidate;
    }

    public void setJobCandidate(JobCandidate jobCandidate) {
        this.jobCandidate = jobCandidate;
    }

    public CandidateDetail getCandidateDetail() {
        return candidateDetail;
    }

    public void setCandidateDetail(CandidateDetail candidateDetail) {
        this.candidateDetail = candidateDetail;
    }

}
