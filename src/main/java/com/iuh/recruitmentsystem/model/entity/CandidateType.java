package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "CandidateTypes")
@ApiModel(description = "All details about the CandidateTypes")
public class CandidateType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated CandidateType ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "The CandidateType Name")
    private String name;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "candidate_id")
    @NotNull
    @ApiModelProperty(notes = "Candidate ID ")
    private Candidate candidate_type;

    @JsonManagedReference
    @OneToMany(mappedBy = "candidateType",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List Candidate Detail: Skill -> [Java,C#]")
    private List<CandidateDetail> candidateDetails;


    public CandidateType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Candidate getCandidate_type() {
        return candidate_type;
    }

    public void setCandidate_type(Candidate candidate_type) {
        this.candidate_type = candidate_type;
    }

    public List<CandidateDetail> getCandidateDetails() {
        return candidateDetails;
    }

    public void setCandidateDetails(List<CandidateDetail> candidateDetails) {
        this.candidateDetails = candidateDetails;
    }
}
