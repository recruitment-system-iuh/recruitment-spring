package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import oracle.sql.TIMESTAMP;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Jobs")
@ApiModel(description = "All details about the Job")
@EntityListeners(AuditingEntityListener.class)
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated Job ID")
    private Long id;

    @Lob
    @Column(length = 65535)
    @ApiModelProperty(notes = "Job Description")
    private String jobDescription;

    @Lob
    @Column(length = 65535)
    @ApiModelProperty(notes = "Job Description")
    private String jobRequirement;


    @Column
    @ApiModelProperty(notes = "Job Name")
    private String jobName;

    @Column
    @ApiModelProperty(notes = "Position")
    private String position;

    @Column
    @ApiModelProperty(notes = "Round Interview")
    private int round;

    @Column
    @ApiModelProperty(notes = "Number of Recruitment")
    private int numberRecruitment;

    @Column
    @CreationTimestamp
    private LocalDateTime createDateTime;

    @Column
    @UpdateTimestamp
    private LocalDateTime updateDateTime;

 //   @JsonManagedReference
    @JsonIgnore
    @OneToMany(mappedBy = "job_config",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List JobConfig")
    private List<JobConfig> jobConfigs;

    @JsonIgnore
 //   @JsonManagedReference
    @OneToMany(mappedBy = "job_candidate",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List Candidate apply Job")
    private List<JobCandidate> jobCandidates;


    public Job() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<JobCandidate> getJobCandidates() {
        return jobCandidates;
    }

    public void setJobCandidates(List<JobCandidate> jobCandidates) {
        this.jobCandidates = jobCandidates;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public List<JobConfig> getJobConfigs() {
        return jobConfigs;
    }

    public void setJobConfigs(List<JobConfig> jobConfigs) {
        this.jobConfigs = jobConfigs;
    }

    public int getNumberRecruitment() {
        return numberRecruitment;
    }

    public void setNumberRecruitment(int numberRecruitment) {
        this.numberRecruitment = numberRecruitment;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public String getJobRequirement() {
        return jobRequirement;
    }

    public void setJobRequirement(String jobRequirement) {
        this.jobRequirement = jobRequirement;
    }
}
