package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "Candidates")
@ApiModel(description = "All details about the Employess. ")
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated Candidate ID")
    private Long id;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id",unique = true)
    @NotNull
    @ApiModelProperty(notes = "Infomation basic Candidate")
    private Person person;

    @JsonIgnore
    //@JsonManagedReference
    @OneToMany(mappedBy = "candidate_type", cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List Header Candidate: Education,Skill,...")
    private List<CandidateType> candidateTypes;

    @JsonManagedReference
    @OneToMany(mappedBy = "candidate_log", cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List log when HR update information")
    private List<Log> logs;

  //  @JsonManagedReference
    @JsonIgnore
    @OneToMany(mappedBy = "candidate",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List Candidate apply Job")
    private List<JobCandidate> jobCandidates;

    public Candidate() {
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<CandidateType> getCandidateTypes() {
        return candidateTypes;
    }

    public void setCandidateTypes(List<CandidateType> candidateTypes) {
        this.candidateTypes = candidateTypes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<JobCandidate> getJobCandidates() {
        return jobCandidates;
    }

    public void setJobCandidates(List<JobCandidate> jobCandidates) {
        this.jobCandidates = jobCandidates;
    }

    public List<Log> getLogs() {
        return logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "id=" + id +
                ", person=" + person +
                ", candidateTypes=" + candidateTypes +
                ", logs=" + logs +
                ", jobCandidates=" + jobCandidates +
                '}';
    }
}
