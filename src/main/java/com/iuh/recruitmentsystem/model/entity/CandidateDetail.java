package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CandidateDetails")
@ApiModel(description = "All details about the CandidateDetail. ")
public class CandidateDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated CandidateDetail ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "The CandidateDetail name: Java,C#,Bank,...")
    private String name;

    /*
    * Định dạng Json. Dựa vào name CandidateType -> Generate 1 cấu trúc động
    * Ví dụ: Type = "Education" -> content = "{GPA: "anc",stydy? "Đại Học Công nghiệp"}"
    * */
    @Column
    @ApiModelProperty(notes = "Format json. Base on Candidate Type. Generate dynamic struct ")
    private String content;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "CandidateType_ID")
    @NotNull
    @ApiModelProperty(notes = "CandidateType: Education, Skills,... ")
    private CandidateType candidateType;

    @JsonManagedReference
    @OneToMany(mappedBy = "candidateDetail",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "Information result mapping CandidateDetail && Jobconfig ")
    private List<JobCandidateDetail> jobCandidateDetails;

    public CandidateDetail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public CandidateType getCandidateType() {
        return candidateType;
    }

    public void setCandidateType(CandidateType candidateType) {
        this.candidateType = candidateType;
    }

    public List<JobCandidateDetail> getJobCandidateDetails() {
        return jobCandidateDetails;
    }

    public void setJobCandidateDetails(List<JobCandidateDetail> jobCandidateDetails) {
        this.jobCandidateDetails = jobCandidateDetails;
    }

}
