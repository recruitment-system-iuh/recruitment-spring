package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Users")
@ApiModel(description = "All details about the User")
public class User {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated User ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "Username")
    private String username;

    @Column
    @JsonIgnore
    @ApiModelProperty(notes = "Password")
    private String password;

    @Column
    @ApiModelProperty(notes = "Rule Permission: Admin, HR, Interviewer")
    private int rule;

    @JsonManagedReference
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id", unique = true)
    @NotNull
    @ApiModelProperty(notes = "Information basic User")
    private Person person;

    @JsonBackReference
    @OneToMany(mappedBy = "user_log", cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List logs User modify Candidate")
    private List<Log> logs;

    //  @JsonManagedReference
    @JsonIgnore
    @OneToMany(mappedBy = "interview_job", cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List Job Interview Permisstion")
    private List<InterviewerJob> interviewerJobs;


    public User(String username, String password, int rule) {
        this.username = username;
        this.password = password;
        this.rule = rule;
    }

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRule() {
        return rule;
    }

    public void setRule(int rule) {
        this.rule = rule;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<Log> getLogs() {
        return logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }

    public List<InterviewerJob> getInterviewerJobs() {
        return interviewerJobs;
    }

    public void setInterviewerJobs(List<InterviewerJob> interviewerJobs) {
        this.interviewerJobs = interviewerJobs;
    }


}
