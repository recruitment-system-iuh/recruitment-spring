package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Persons")
@ApiModel(description = "All details about the Person")
public class Person  {

    @Id
    @Column(name = "person_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated Person ID")
    private Long id;

    @Column
    @ApiModelProperty(notes = "")
    private String firstName;

    @Column
    @ApiModelProperty(notes = "")
    private String lastName;

    @Column
    @ApiModelProperty(notes = "")
    private String birthday;

    @Column
    @ApiModelProperty(notes = "")
    private String address;

    @Column
    @ApiModelProperty(notes = "")
    private String phoneNumber;

    @Column
    @ApiModelProperty(notes = "")
    private String gender;

    @Column
    @ApiModelProperty(notes = "")
    private String email;

    @Column
    @ApiModelProperty(notes = "")
    private String objective;

    @Column
    @ApiModelProperty(notes = "")
    private String website;

    @Column
    @ApiModelProperty(notes = "")
    private String jobTitle;

    @JsonBackReference
    @OneToOne(mappedBy = "person",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @ApiModelProperty(notes = "")
    private User user;

    public Person() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
}
