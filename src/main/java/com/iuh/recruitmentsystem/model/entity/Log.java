package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Logs")
@ApiModel(description = "All details about the log")
public class Log  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated Log ID")
    private Long id;

/*    @Column
    @ApiModelProperty(notes = "Value out")
    private String changeValue;

    @ApiModelProperty(notes = "Value New")
    @Column
    private String toValue;

    @Column
    @ApiModelProperty(notes = "Column Update")
    private String updateField;*/


    @Column
    @CreationTimestamp
    private LocalDateTime createDateTime;

   // @JsonBackReference
    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    @ApiModelProperty(notes = "")
    private User user_log;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "candidate_id")
    @NotNull
    @ApiModelProperty(notes = "")
    private Candidate candidate_log;

    @JsonManagedReference
    @OneToMany(mappedBy = "logs", cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List Logs Detail")
    private List<LogDetail> logDetails;


    public Log() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public User getUser_log() {
        return user_log;
    }

    public void setUser_log(User user_log) {
        this.user_log = user_log;
    }

    public Candidate getCandidate_log() {
        return candidate_log;
    }

    public void setCandidate_log(Candidate candidate_log) {
        this.candidate_log = candidate_log;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public List<LogDetail> getLogDetails() {
        return logDetails;
    }

    public void setLogDetails(List<LogDetail> logDetails) {
        this.logDetails = logDetails;
    }

}
