package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "InterviewerJobs")
@ApiModel(description = "All details about the InterviewJob")
public class InterviewerJob {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated InterviewJob ID")
    private Long id;

    @ApiModelProperty(notes = "Rule Permission Supporter or Interviewer")
    private int rule;

    @ApiModelProperty(notes = "Rule Permission Supporter or Interviewer")
    private int round;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "JobCand_ID")
    @NotNull
    @ApiModelProperty(notes = "JobCandi Permisstion")
    private JobCandidate jobCandidate;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "User_ID")
    @ApiModelProperty(notes = "Interviewr Permisstion")
    private User interview_job;


    public InterviewerJob() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getInterview_job() {
        return interview_job;
    }

    public void setInterview_job(User interview_job) {
        this.interview_job = interview_job;
    }

    public JobCandidate getJobCandidate() {
        return jobCandidate;
    }

    public int getRule() {
        return rule;
    }

    public void setRule(int rule) {
        this.rule = rule;
    }

    public void setJobCandidate(JobCandidate jobCandidate) {
        this.jobCandidate = jobCandidate;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    @Override
    public String toString() {
        return "InterviewerJob{" +
                "id=" + id +
                ", rule=" + rule +
                ", jobCandidate=" + jobCandidate +
                ", interview_job=" + interview_job +
                '}';
    }
}
