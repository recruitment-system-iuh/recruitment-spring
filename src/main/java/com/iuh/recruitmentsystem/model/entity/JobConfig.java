package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "JobConfigs")
@ApiModel(description = "All details about the JobConfig")
public class JobConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated JobConfig ID")
    private Long id;



    @Column
    @Lob
    @ApiModelProperty(notes = "Data from template generate dynamic form for ConfigType")
    private String content;

   // @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "Config_ID")
    @NotNull
    @ApiModelProperty(notes = "")
    private Config config;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "Job_ID")
    @NotNull
    @ApiModelProperty(notes = "")
    private Job job_config;

    @JsonManagedReference
    @OneToMany(mappedBy = "jobConfig",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "")
    private List<JobCandidateDetail> jobCandidateDetails;

    public JobConfig() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public Job getJob_config() {
        return job_config;
    }

    public void setJob_config(Job job_config) {
        this.job_config = job_config;
    }

    public List<JobCandidateDetail> getJobCandidateDetails() {
        return jobCandidateDetails;
    }

    public void setJobCandidateDetails(List<JobCandidateDetail> jobCandidateDetails) {
        this.jobCandidateDetails = jobCandidateDetails;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

