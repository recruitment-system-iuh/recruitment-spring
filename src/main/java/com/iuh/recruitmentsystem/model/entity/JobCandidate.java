package com.iuh.recruitmentsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "JobCandidates")
@ApiModel(description = "All details about the JobCandidate")
public class JobCandidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated JobCandidate ID")
    private Long id;

    /*
    * id JobCandidateInterviewer. Dựa vào id để xử lý việc hiện tai Candidate đang phòng vấn
    * vòng nào. Nhằm update status interviewer
    * */
    @Column
    @ApiModelProperty(notes = "JobCandidateInterviewerID -> Update status Interviewer")
    private long currentRound;
    /*
    * Dùng để update và phân quyền cho người phỏng vấn nào có quyền feedback
    * Và quyền đối với vòng phỏng vấn nào
    * Ex: ABC123-1 -> ABC123: id User. 1: Vòng 1
    * */
//    @Column
//    @ApiModelProperty(notes = "Update && Permission Interview Feedback in case Many Interviewer")
//    private long idInterviewRound;

    /*
    * Các trạng thái:
    * - wait: Đã apply vào job. Đang đợi review
    * - viewed: Đã review CV. Đợi lịch phỏng vấn
    * - Interviewing:Đang phỏng vấn
    * - Offering:
    * - Probation:
    * */
    @Column
    @ApiModelProperty(notes = "Status Candidate: wait,viewed,Interviewing,Offering,Probation")
    private String status;

    /*
    * Hiện thị status của vòng phòng vấn hiện tại dựa vào currentRound
    * Nếu vòng 1, kết quả Pass -> Display Vòng 2 -> Waiting
    * Vòng 2, Interviewing -> Display vòng 2 -> Interviewing
    * * Trạng thái
    *   - waiting: đang đợi xếp lịch
    *   - interviewing: đang phỏng vấn
    * */
//    @Column
//    @ApiModelProperty(notes = "Status Interview current")
//    private String statusInterview;

    @Column
    @ApiModelProperty(notes = "Position Candidate Appy Job")
    private String position;

    @Column
    @CreationTimestamp
    private LocalDateTime createDateTime;

    @Column
    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    @Column
    @ApiModelProperty(notes = "URL CV Candidate AWS S3")
    private String urlCV;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "Candidate_ID")
    @NotNull
    @ApiModelProperty(notes = "Candidate")
    private Candidate candidate;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "Job_ID")
    @NotNull
    @ApiModelProperty(notes = "Job")
    private Job job_candidate;

    @JsonIgnore
    @OneToMany(mappedBy = "JobCandidate",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List JobCandidateInterviews: Schedule Interview")
    private List<JobCandidateInterview> jobCandidateInterviews;

    @JsonManagedReference
    @OneToMany(mappedBy = "jobCandidate",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "Result evaluate CandidateDetail && JobConfig ")
    private List<JobCandidateDetail> jobCandidateDetails;

    // @JsonManagedReference
    @JsonIgnore
    @OneToMany(mappedBy = "jobCandidate",cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "List InterviewJob Permisstion")
    private List<InterviewerJob>  interviewerJobs;

    public JobCandidate() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(long currentRound) {
        this.currentRound = currentRound;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Job getJob_candidate() {
        return job_candidate;
    }

    public void setJob_candidate(Job job_candidate) {
        this.job_candidate = job_candidate;
    }

    public List<JobCandidateInterview> getJobCandidateInterviews() {
        return jobCandidateInterviews;
    }

    public void setJobCandidateInterviews(List<JobCandidateInterview> jobCandidateInterviews) {
        this.jobCandidateInterviews = jobCandidateInterviews;
    }

    public List<JobCandidateDetail> getJobCandidateDetails() {
        return jobCandidateDetails;
    }

    public void setJobCandidateDetails(List<JobCandidateDetail> jobCandidateDetails) {
        this.jobCandidateDetails = jobCandidateDetails;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public String getUrlCV() {
        return urlCV;
    }

    public void setUrlCV(String urlCV) {
        this.urlCV = urlCV;
    }

    public List<InterviewerJob> getInterviewerJobs() {
        return interviewerJobs;
    }

    public void setInterviewerJobs(List<InterviewerJob> interviewerJobs) {
        this.interviewerJobs = interviewerJobs;
    }
}
