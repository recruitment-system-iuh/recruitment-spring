package com.iuh.recruitmentsystem.model;

public class Constants {

    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 86400; // 24*60*60
    public static final String SIGNING_KEY = "ptbao123";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
