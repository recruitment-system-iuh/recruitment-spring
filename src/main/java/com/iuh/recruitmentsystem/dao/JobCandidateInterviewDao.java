package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.hellper.CustormScheduleDetailDto;
import com.iuh.recruitmentsystem.model.entity.JobCandidateInterview;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobCandidateInterviewDao extends CrudRepository<JobCandidateInterview, Long> {


    @Query(value = "select j.ID,j2.ROUND,j2.STATUS,j2.DATETIME,j2.FEEDBACK, j2.ADDRESS, P.FIRSTNAME, P.LASTNAME, interview.RULE from\n" +
            "  JOBCANDIDATES j\n" +
            "  join JOBCANDIDATEINTERVIEWS J2 on J2.JOBCAND_ID = j.ID\n" +
            "  join INTERVIEWERJOBS interview on interview.JOBCAND_ID = j.ID\n" +
            "  join USERS U on U.USER_ID = interview.USER_ID\n" +
            "  join PERSONS P on U.PERSON_ID = P.PERSON_ID" +
            " where j.ID = :jobcandid and interview.RULE = 1 and  j2.ROUND = interview.ROUND",nativeQuery = true)
    List<CustormScheduleDetailDto> findScheduleDetailByJobCandiID(@Param("jobcandid") long jobcandid);

}
