package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends CrudRepository<User, Long> {

    User findByUsername(String username);
    List<User> findAllByRule(int rule);
}
