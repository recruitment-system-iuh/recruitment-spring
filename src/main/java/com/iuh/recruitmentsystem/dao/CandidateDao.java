package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.Candidate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateDao extends CrudRepository<Candidate,Long> {

    @Query(value = "select CandType.NAME HEADER, CandDetail.NAME SUBHEADER, CandDetail.CONTENT CONTENT\n" +
            "from   CANDIDATES candi\n" +
            "       join CANDIDATETYPES CandType on candi.ID = CandType.CANDIDATE_ID\n" +
            "       join CANDIDATEDETAILS CandDetail on CandType.ID = CandDetail.CANDIDATETYPE_ID\n" +
            "where CANDIDATE_ID = :candId",nativeQuery = true)
    List<Object[]> getResumeDetailByID(@Param("candId") long candId);
}
