package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.hellper.CustormJobCandidateScheduleByID;
import com.iuh.recruitmentsystem.model.entity.JobCandidate;
import com.iuh.recruitmentsystem.model.hellper.CustormReportJobCandidate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobCandidateDao extends CrudRepository<JobCandidate, Long> {


    List<JobCandidate> findJobCandidatesByStatus(String status);

    @Query(value = "select job.ID,J.ID as JOBCANDID,job.JOBNAME,job.POSITION,job.ROUND as JOBROUND,job.NUMBERRECRUITMENT," +
            "P.FIRSTNAME, P.LASTNAME,P.EMAIL,p.PHONENUMBER,J.CREATEDATETIME,J2.ROUND as INTERVIEWROUND ," +
            "J2.STATUS, J.STATUS as JOBCANDSTATUS\n" +
            "from JOBS job\n" +
            "    join JOBCANDIDATES J on job.ID = J.JOB_ID\n" +
            "    join CANDIDATES C2 on J.CANDIDATE_ID = C2.ID\n" +
            "    join PERSONS P on C2.PERSON_ID = P.PERSON_ID\n" +
            "    join JOBCANDIDATEINTERVIEWS J2 on J.CURRENTROUND = J2.ID\n" +
            "where job.ID = :jobid and ( J.STATUS = 'interview' or J.STATUS = 'viewed') ",nativeQuery = true)
    List<CustormJobCandidateScheduleByID> findInterviewCandiByJobID(@Param("jobid") long jobid);

    @Query(value = "select\n" +
            "    (select count(*)  from JOBCANDIDATES ) as total\n" +
            "   ,(select count(*) from JOBCANDIDATES where STATUS = 'waited') waited\n" +
            "   ,(select count(*) from JOBCANDIDATES where STATUS = 'viewed') viewed\n" +
            "   ,(select count(*) from JOBCANDIDATES where STATUS = 'interview') interview\n" +
            "   ,(select count(*) from JOBS) jobnew\n" +
            "   ,(select count(*) from JOBCANDIDATES where STATUS = 'offer') offer\n" +
            "from JOBCANDIDATES\n" +
            "where 1 = 1\n" +
            "  and CREATEDATETIME > (sysdate - 30)\n" +
            "  and ROWNUM = 1", nativeQuery = true)
    List<CustormReportJobCandidate> reportJobCandiByOneMonth();

    @Query(value = "select J.JOBNAME, J.POSITION as P_RECRUITMENT\n" +
            "     , P.FIRSTNAME, P.LASTNAME, P.EMAIL, JCA.POSITION as P_APPLY\n" +
            "     , JCAI.QUESTIONINTERVIEW,JCA.URLCV,JCAI.ROUND, JCAI.ADDRESS\n" +
            "     , TO_CHAR(JCAI.DATETIME,'DD-MM-YYYY HH24:MI:SS') as Date_Interview\n" +
            "from JOBCANDIDATES JCA\n" +
            "  join JOBS J on JCA.JOB_ID = J.ID\n" +
            "  join CANDIDATES CA on JCA.CANDIDATE_ID = CA.ID\n" +
            "  join PERSONS P on CA.PERSON_ID = P.PERSON_ID\n" +
            "  join JOBCANDIDATEINTERVIEWS  JCAI on JCA.CURRENTROUND = JCAI.ID\n" +
            "  where JCA.ID = :jobcandID", nativeQuery = true)
    List<Object[]> getdataFeedback(@Param("jobcandID") long jobcandID);

}
