package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.hellper.CustormConfigTypeDto;
import com.iuh.recruitmentsystem.model.entity.ConfigType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConfigTypeDao extends CrudRepository<ConfigType,Long> {

    @Query(value = "select o.id, o.name , t.nameModel, t.Property1, t.Property2 from configtypes o, json_table(o.content, '$.configtypes[*]'" +
            "columns (nameModel varchar path '$.name',Property1 varchar path '$.property1.property',Property2 varchar path '$.property2.property'," +
            "type1 varchar path '$.property1.type',type2 varchar path '$.property2.type')) t " +
            "where o.id=?1",
    nativeQuery = true)
    List<CustormConfigTypeDto> getModelById(long id);
}
