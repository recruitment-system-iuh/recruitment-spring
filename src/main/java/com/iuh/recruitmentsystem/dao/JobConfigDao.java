package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.JobConfig;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobConfigDao extends CrudRepository<JobConfig,Long> {

    @Query(value = "select JCF.CONTENT,CFG.CONFIGTYPE_ID,CFG.NAME CFG_NAME,CFG_TP.NAME CFG_TP_NAME\n" +
            "from JOBS job\n" +
            "       join JOBCONFIGS JCF on job.ID = JCF.JOB_ID\n" +
            "       join CONFIGS CFG on JCF.CONFIG_ID = CFG.ID\n" +
            "       join CONFIGTYPES CFG_TP on CFG.CONFIGTYPE_ID = CFG_TP.ID\n" +
            "where JOB_ID = :jobID",nativeQuery = true)
    List<Object[]> findAllByJobId(@Param("jobID") long jobID);


}
