package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.Config;
import com.iuh.recruitmentsystem.model.entity.Job;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigDao extends CrudRepository<Config,Long> {

}
