package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.hellper.CustormInterviewJobDto;
import com.iuh.recruitmentsystem.model.entity.InterviewerJob;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InterviewJobDao extends CrudRepository<InterviewerJob,Long> {

    @Query(value = "select distinct job.ID, job.JOBNAME, job.POSITION,job.ROUND as JOBROUND, P.FIRSTNAME, P.LASTNAME, P.EMAIL,I.JOBCAND_ID,jobcan.CREATEDATETIME,JV.ROUND as JVROUND, JV.ID as JCI_ID \n" +
            "  from JOBS job\n" +
            "  join JOBCANDIDATES jobcan on jobcan.JOB_ID = job.ID\n" +
            "  join INTERVIEWERJOBS I on I.JOBCAND_ID = jobcan.ID\n" +
            "  join CANDIDATES C2 on jobcan.CANDIDATE_ID = C2.ID\n" +
            "  join PERSONS P on C2.PERSON_ID = P.PERSON_ID\n" +
            "  join JOBCANDIDATEINTERVIEWS JV on jobcan.ID = JV.JOBCAND_ID" +
            "  where I.USER_ID = :userid and JV.STATUS = 'interview' and (:jobid = 0 or job.ID = :jobid) ",nativeQuery = true)
    List<CustormInterviewJobDto> findAllJobByUserID(@Param("userid") long userId, @Param("jobid") long jobId);

}
