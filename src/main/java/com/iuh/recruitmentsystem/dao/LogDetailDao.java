package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.Log;
import com.iuh.recruitmentsystem.model.entity.LogDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogDetailDao extends CrudRepository<LogDetail,Long> {

}
