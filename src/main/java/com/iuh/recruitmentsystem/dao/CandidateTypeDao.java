package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.CandidateType;
import org.springframework.data.repository.CrudRepository;

public interface CandidateTypeDao extends CrudRepository<CandidateType, Long> {

}
