package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonDao extends CrudRepository<Person, Long> {

    Person findById(long id);
}
