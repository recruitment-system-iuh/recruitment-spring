package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.Config;
import com.iuh.recruitmentsystem.model.entity.Log;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogDao extends CrudRepository<Log,Long> {

    @Query(value = "select * from logs l where l.candidate_id= ?1",nativeQuery = true)
    List<Log> findAllByCandidateID(long candidate_id);
}
