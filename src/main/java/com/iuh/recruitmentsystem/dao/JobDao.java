package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.Job;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobDao extends CrudRepository<Job,Long> {

}
