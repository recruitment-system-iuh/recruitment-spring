package com.iuh.recruitmentsystem.dao;

import com.iuh.recruitmentsystem.model.entity.CandidateDetail;
import com.iuh.recruitmentsystem.model.entity.CandidateType;
import org.springframework.data.repository.CrudRepository;

public interface CandidateDetailDao extends CrudRepository<CandidateDetail, Long> {

}
