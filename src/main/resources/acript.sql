-- Person User
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Lương Ngọc Quyến', '12/03/2000','PT', 'Bão', '0379608838','ptbao@gmail.com','male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Nguyễn Văn Trỗi', '12/05/1996','Trần', 'Tèo', '092344584','tranteo123@gmail.com','female');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Trần Hưng Đạo', '21/04/1997','Lý', 'Tĩnh', '0371642145','lytinh456@gmail.com','male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Nguyên Văn Luong', '21/04/1997','Nguyễn', 'Sơn', '0124222555','trannguyen@gmail.com','male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('789 Lương Ngoc Quyên', '12/03/1994','Võ', 'Thái Phúc', '0907555442','vulinh@gmail.com','female');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('1101 Phan Văn Trị', '12/05/1996','Trần', 'Văn Hiếu', '0369635432','chicuong@gmail.com','male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Nguyễn Thị Minh Khai', '21/04/1997','Hoàng', 'Văn Đoàn', '0124222555','luuthong@gmail.com','female');

--Person Candidate
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('789 Phan Xích Long', '12/03/1994','Huỳnh', 'Sơn', '0907555442','huynson111@gmail.com','male','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('1101 Quang Trung. Gò Vấp', '12/05/1996','Trần', 'Bùm', '0369635432','tranbum12@gmail.com','female','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('123 Hai Bà Trưng', '21/04/1997','Lưu', 'Hoàng', '0124222555','luuhoang199@gmail.com','female','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('454 Huỳnh Khương An', '12/03/2010','Hồ', 'Quốc Vững', '0378825441','hovung@gmail.com','male','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('123 Nguyễn Văn Cừ', '12/05/1996','Nguyễn', 'Thu Thảo', '092344584','thuthao123@gmail.com','female','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('123 Phan Văn Trị', '21/04/1997','Nguyễn', 'Văn Cường', '0371642145','vancuong1999@gmail.com','male','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('123 Nguyên Văn Luong', '21/04/1997','Trân', 'Nguyên', '0124222555','trannguyen@gmail.com','male','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('789 Lương Ngoc Quyên', '12/03/1994','Pham', 'Vu Linh', '0907555442','vulinh@gmail.com','female','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('1101 Phan Văn Trị', '12/05/1996','Trần', 'Chi Cương', '0369635432','chicuong@gmail.com','male','http://35.240.202.3/client/home','Developer','Everything');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER,WEBSITE,JOBTITLE,OBJECTIVE) VALUES ('123 Nguyễn Thị Minh Khai', '21/04/1997','Lưu', 'Thông', '0124222555','luuthong@gmail.com','female','http://35.240.202.3/client/home','Developer','Everything');

-- User
-- Rule:
-- + 0: HR
-- + 1: Interview
-- + 2: HR Admin
INSERT INTO users (username, password, rule, person_id) VALUES ('alex123','$2a$04$4vwa/ugGbBVDvbWaKUVZBuJbjyQyj6tqntjSmG8q.hi97.xSdhj/2', 0, 1);
INSERT INTO users (username, password, rule, person_id)  VALUES ('tom234', '$2a$04$QED4arFwM1AtQWkR3JkQx.hXxeAk/G45NiRd3Q4ElgZdzGYCYKZOW', 1, 2);
INSERT INTO users (username, password, rule, person_id)  VALUES ('aadam', '$2a$04$WeT1SvJaGjmvQj34QG8VgO9qdXecKOYKEDZtCPeeIBSTxxEhazNla', 2, 3);
INSERT INTO users (username, password, rule, person_id) VALUES ('nguyenson123','$2a$10$p4uLy0ttVui5Px.1nppz1e.PQdR8FyNR3G6eSCCfoFsM2pvHaVMAi', 1, 4); -- pass = 123
INSERT INTO users (username, password, rule, person_id)  VALUES ('thaiphuc123', '$2a$10$p4uLy0ttVui5Px.1nppz1e.PQdR8FyNR3G6eSCCfoFsM2pvHaVMAi', 1, 5);-- pass = 123
INSERT INTO users (username, password, rule, person_id)  VALUES ('vanhieu123', '$2a$10$p4uLy0ttVui5Px.1nppz1e.PQdR8FyNR3G6eSCCfoFsM2pvHaVMAi', 1, 6);-- pass = 123
INSERT INTO users (username, password, rule, person_id)  VALUES ('vandoan123', '$2a$10$p4uLy0ttVui5Px.1nppz1e.PQdR8FyNR3G6eSCCfoFsM2pvHaVMAi', 1, 7);-- pass = 123

-- Candidate
INSERT INTO candidates(person_id) VALUES(8);
INSERT INTO candidates(person_id) VALUES(9);
INSERT INTO candidates(person_id) VALUES(10);
INSERT INTO candidates(person_id) VALUES(11);
INSERT INTO candidates(person_id) VALUES(12);
INSERT INTO candidates(person_id) VALUES(13);
INSERT INTO candidates(person_id) VALUES(14);
INSERT INTO candidates(person_id) VALUES(15);
INSERT INTO candidates(person_id) VALUES(16);
INSERT INTO candidates(person_id) VALUES(17);

-- Job
INSERT INTO JOBS(JOBDESCRIPTION,JOBREQUIREMENT,JOBNAME,POSITION,ROUND,NUMBERRECRUITMENT,CREATEDATETIME,UPDATEDATETIME)
VALUES ('Develop and implement machine learning application for real projects.Research, discuss and propose solutions for machine learning projects.Work with senior machine learning engineers and Professors in university from idea to development.Directly involve in building several current ML projects:SI Automation: extract information from scanned or image documents with various format.CCTV camera analysis in container terminal for safety and security.Chatbot development: develop chatbot application.	Develop machine learning application.',
        'Strong experience in software development.Have knowledge about machine learning and working experience from 1-2 years.Know how to build and implement machine learning, deep learning application.Have experience in developing machine learning application is a plus.Experience in using machine learning frameworks such as Tensorflow, Keras.Be familiar with Image Processing and Computer Vision (know OpenCV) is a plus.Strong programming mindset in one of popular languages like Python, Javacript, Java.Self-research and strong teamwork skills.High responsibility and pro working attitude.',
        'Machine Learning Developer & Researcher',
        'Junior,Senior',
        3,
        20,
        CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO JOBS(JOBDESCRIPTION,JOBREQUIREMENT,JOBNAME,POSITION,ROUND,NUMBERRECRUITMENT,CREATEDATETIME,UPDATEDATETIME)
VALUES ('Will be trained and work on Oracle, Java Spring, Action Script, HTML5, AngularJs.Join one of 2 teams: Product/ MaintenanceProduct: new development projectsMaintenance: the developed projects which related to the import and export process.The system has been operating in the US, Japan, Korea and some European countries, etc.After being clear about the purpose and meaning of the job, everyone will use the internal management system to manage and complete the work.The task will be done following the close procedure to ensure the high quality before reaching to the user through the internal system',
        'Entry, Junior and Senior.Plus to have experience working with a good Java programming.Able to analyze well.Good attitude and responsibility.Have good programming and programming skills.English communication: reading and writing. Fluent speaking, listening is an advantage',
        'JAVA SOFTWARE ENGINEER','Entry,Junior, Senior',
        2,
        50,
        CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO JOBS(JOBDESCRIPTION,JOBREQUIREMENT,JOBNAME,POSITION,ROUND,NUMBERRECRUITMENT,CREATEDATETIME,UPDATEDATETIME) VALUES ('Receive requests from world-wide customers via email, website, phone call.Support to resolve customer’s problems encountered when using the company''s system.Most of customers come from US, Korea, Japan, Singapore, India, China, Turkey, etc.',
        'Ability to analyze, logical thinking.Good English; ability to write, read well.Can use office software (Word, Excel, Outlook).',
        'BUSINESS ANALYSIS (GLOBAL HELPDESK)','HELPDESK',
        3,
        40,
        CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- JobCandidate

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(1,1,'Senior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample1.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(1,2,'Junior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(1,3,'HELPDESK','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(2,3,'HELPDESK','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(2,2,'Junior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(3,1,'Senior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(3,3,'HELPDESK','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(4,1,'Senior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample1.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(4,2,'Senior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(4,3,'HELPDESK','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(5,2,'Junior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(5,1,'Senior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(5,3,'HELPDESK','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(6,3,'HELPDESK','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(6,2,'Junior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(6,1,'Junior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');


INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(7,1,'Senior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(7,3,'HELPDESK','viewed',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(7,2,'Junior','viewed',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(8,1,'Senior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample1.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(8,2,'Junior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(9,3,'HELPDESK','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(9,2,'Senior','waited',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(10,2,'Senior','viewed',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,CREATEDATETIME,URLCV) VALUES(10,3,'HELPDESK','viewed',0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');


-- JobCandidateInterview
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (3,1,'pass',CURRENT_TIMESTAMP,'Verry Good','Scepta Building');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (4,1,'pass',CURRENT_TIMESTAMP,'Bad','Scepta Building');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (4,2,'interview',CURRENT_TIMESTAMP,'','Scepta Building');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (6,1,'pass',CURRENT_TIMESTAMP,'Verry Good','Scepta Building');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (6,2,'interview',CURRENT_TIMESTAMP,'','Nam Hai Building');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (8,1,'pass',CURRENT_TIMESTAMP,'Verry Good','Scepta Building');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (8,2,'pass',CURRENT_TIMESTAMP,'Verry Good','Nam Hai Building');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (9,1,'pass',CURRENT_TIMESTAMP,'Verry Good','Scepta Building');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS,QUESTIONINTERVIEW) VALUES (9,2,'interview',CURRENT_TIMESTAMP,'','Scepta Building','[{"type":"hidden","name":"hidden-1562575764406"},{"type":"radio-group","label":"Radio Group","name":'' + ''"radio-group-1562575764870","values":[{"label":"Option 1","value":"option-1"},{"label":"Option 2","value":"option-2"},'' + ''{"label":"Option 3","value":"option-3"}]},{"type":"text","label":"Text Field","className":"form-control",'' + ''"name":"text-1562575765370","subtype":"text"}]');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (11,1,'pass',CURRENT_TIMESTAMP,'Verry Good','Nam Hai Building');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (16,1,'pass',CURRENT_TIMESTAMP,'Good','Scepta Building');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS,QUESTIONINTERVIEW) VALUES (16,2,'interview',CURRENT_TIMESTAMP,'','Scepta Building','[{"type":"hidden","name":"hidden-1562575764406"},{"type":"radio-group","label":"Radio Group","name":'' + ''"radio-group-1562575764870","values":[{"label":"Option 1","value":"option-1"},{"label":"Option 2","value":"option-2"},'' + ''{"label":"Option 3","value":"option-3"}]},{"type":"text","label":"Text Field","className":"form-control",'' + ''"name":"text-1562575765370","subtype":"text"}]');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (20,1,'pass',CURRENT_TIMESTAMP,'Verry Good','Nam Hai Building');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (21,1,'pass',CURRENT_TIMESTAMP,'Verry Good','Nam Hai Building');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (18,1,'wait','','','');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (19,1,'wait','','','');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (24,1,'wait','','','');

INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK,ADDRESS) VALUES (25,1,'wait','','','');

UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 1, STATUS = 'interview' WHERE t."ID" = 3;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 3, STATUS = 'interview' WHERE t."ID" = 4;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 5, STATUS = 'interview' WHERE t."ID" = 6;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 7, STATUS = 'interview' WHERE t."ID" = 8;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 9, STATUS = 'interview' WHERE t."ID" = 9;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 10, STATUS = 'interview' WHERE t."ID" = 11;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 12, STATUS = 'interview' WHERE t."ID" = 16;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 13, STATUS = 'interview' WHERE t."ID" = 20;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 14, STATUS = 'interview' WHERE t."ID" = 21;
-- -- status : viewed
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 15 WHERE t."ID" = 18;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 16 WHERE t."ID" = 19;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 17 WHERE t."ID" = 24;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 18 WHERE t."ID" = 25;

-- Interview Job
-- feedback
INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (2,3,1,1);

INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (4,4,1,1);
INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (5,4,1,2);

INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (5,6,1,1);
INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (2,6,1,2);

INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (2,8,1,1);
INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (6,8,1,2);

INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (7,9,1,1);
INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (6,9,1,2);

INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (5,11,1,1);

INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (6,16,1,1);
INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (5,16,1,2);

INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (2,20,1,1);

INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (6,21,1,1);
-- supporter jobcandi 3
INSERT INTO INTERVIEWERJOBS (USER_ID,JOBCAND_ID,RULE,ROUND) VALUES (7,3,2,1);


-- Interview Job
-- User ID = 2: Interviewr -> tom234
-- INSERT  INTO INTERVIEWERJOBS(USER_ID,JOBCAND_ID,RULE) values (1,2);
-- INSERT  INTO INTERVIEWERJOBS(USER_ID,JOBCAND_ID,RULE) values (2,2);
-- CandidateType
/*INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('EDUCATION',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('SKILLS',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('JOBWORK',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('ACCOMPLISHMENTS',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('PROJECT',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('OTHERS',1);
-- INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('PROJECT',1);

INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('EDUCATION',2);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('SKILLS',2);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('WORK EXPERIENCE',2);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('LANGUAGE',2);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('INTERESTS',2);

INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('EDUCATION',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('SKILLS',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('WORK EXPERIENCE',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('CERTIFICATES',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('LANGUAGE',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('INTERESTS',3);


-- CandidateDetail
-- Education 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"University":"DHCN","Major":"Software","GPA":"3.6/4","fromYear":"2010","toYear":"2015"}','Đại Học Công Nghiệp',1);
-- SKILLS 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"point":"2"}','JAVA',2);
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"point":"3"}','C#',2);
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"point":"4"}','JAVASCRIPT',2);
-- WORK EXPERIENCE 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"Company":"TMA Solution","Year":"3"}','JAVA',3);
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"Company":"Sacombank","Year":"1"}','Bank',3);
-- CERTIFICATES 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"Year":"2018","IssuedBy":"IUH","Point":"800",}','Toeic',4);
-- LANGUAGE 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"Level":"1"}','English',5);
-- INTERESTS 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{}','Reading Book',6);

INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{}','Game',6);*/



INSERT INTO configtypes(name,content) VALUES('Technical','{"configtypes":[{"name":"Skill","properties":[{"type":"number","property":"Point","id":"1"}]},{"name":"WorkExperience","properties":[{"type":"number","property":"Year","id":"2"}]}]}');
INSERT INTO configtypes(name,content) VALUES('Bussiness','{"configtypes":[{"name":"TypeOfBussiness","properties":[{"type":"text","property":"nameBussiness","id":"1"},{"type":"number","property":"Year","id":"2"}]}]}');
INSERT INTO configtypes(name,content) VALUES('SoftwareSkill','{"configtypes":[{"name":"Languages","properties":[{"type":"text","property":"Language","id":"1"},{"type":"number","property":"level","id":"2"}]},{"name":"SoftSkill","properties":[{"type":"text","property":"nameSoft","id":"3"}]}]}');

-- Technical
INSERT INTO configs (name,configtype_id) VALUES('C#',1);
INSERT INTO configs (name,configtype_id) VALUES('JAVA',1);
INSERT INTO configs (name,configtype_id) VALUES('C++',1);

-- Bussiness
INSERT INTO configs (name,configtype_id) VALUES('Bank',2);
INSERT INTO configs (name,configtype_id) VALUES('Shipping Line',2);
INSERT INTO configs (name,configtype_id) VALUES('Insurrance',2);

-- Software Skill
INSERT INTO configs (name,configtype_id) VALUES('English',3);
INSERT INTO configs (name,configtype_id) VALUES('Communication',3);
INSERT INTO configs (name,configtype_id) VALUES('Research',3);
INSERT INTO configs (name,configtype_id) VALUES('Toeic',3);

-- JobConfig
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Point" : "1","Year" : "1"}',1,1);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Point" : "3","Year" : "2"}',2,1);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Point" : "3","Year" : "1"}',3,1);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"nameBussiness" : "Shipping ","Year" : "2"}',5,1);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"nameBussiness" : "Insurrance ","Year" : "4"}',6,1);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Language" : "English","level" : "4","nameSoft" : "Languages "}',7,1);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Point" : "1","Year" : "1"}',1,2);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Point" : "2","Year" : "1"}',2,2);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Language" : "English","level" : "4","nameSoft" : "Languages "}',7,2);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"nameBussiness" : "Shipping ","Year" : "2"}',5,2);


Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Point" : "3","Year" : "2"}',3,3);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"nameBussiness" : "Shipping ","Year" : "2"}',5,3);
Insert into JOBCONFIGS (CONTENT,CONFIG_ID,JOB_ID) values ('{"Language" : "English","level" : "4","nameSoft" : "Languages "}',7,3);

-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,1,'{}');
-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,1,'{}');
-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,1,'{}');
-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,2,'{}');
-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,3,'{}');

alter table configtypes add constraint configtypes_json_chk CHECK (CONTENT IS JSON) ENABLE NOVALIDATE;
alter table JOBCONFIGS add constraint JOBCONFIGS_json_chk CHECK (CONTENT IS JSON) ENABLE NOVALIDATE;
alter table CANDIDATEDETAILS add constraint CANDIDATEDETAILS_json_chk CHECK (CONTENT IS JSON) ENABLE NOVALIDATE;

-- select p.FIRSTNAME,p.LASTNAME,p.ADDRESS,p.BIRTHDAY,p.PHONENUMBER, L.CHANGEVALUE,L.TOVALUE,L.CREATEDATETIME
--   from CANDIDATES can join PERSONS P on can.PERSON_ID = P.PERSON_ID
--     join LOGS L on can.ID = L.CANDIDATE_ID
--
-- select * from JOBCONFIGS cfg  where cfg.JOB_ID = 21
--
-- select job.JOBNAME from JOBS job join JOBCANDIDATES jcnd on job.ID = jcnd.JOB_ID
--       join JOBCANDIDATEINTERVIEWS jcin  on jcnd.ID = jcin.JOBCAND_ID
-- where job.ID = 3 and ( jcnd.STATUS = 'viewed' or jcnd.STATUS='interviewing')

select job.ID,J.ID as jobCandID,job.JOBNAME,job.POSITION,job.ROUND as JOBROUND,job.NUMBERRECRUITMENT,P.FIRSTNAME, P.LASTNAME,P.EMAIL,p.PHONENUMBER,J.CREATEDATETIME,J2.ROUND as INTERVIEWROUND ,J2.STATUS, J.STATUS as JOBCANDSTATUS
from JOBS job
       join JOBCANDIDATES J on job.ID = J.JOB_ID
       join CANDIDATES C2 on J.CANDIDATE_ID = C2.ID
       join PERSONS P on C2.PERSON_ID = P.PERSON_ID
       join JOBCANDIDATEINTERVIEWS J2 on J.CURRENTROUND = J2.ID
where job.ID = 3;

-- findScheduleDetailByJobCandiID
select j.ID,j2.ROUND,j2.STATUS,j2.DATETIME,j2.FEEDBACK, j2.ADDRESS,
       P.FIRSTNAME, P.LASTNAME, interview.RULE, interview.ROUND
from JOBCANDIDATES j
       join JOBS job on job.ID = j.JOB_ID
       join JOBCANDIDATEINTERVIEWS J2 on J2.JOBCAND_ID = j.ID
       join INTERVIEWERJOBS interview on interview.JOBCAND_ID = j.ID
       join USERS U on U.USER_ID = interview.USER_ID
       join PERSONS P on U.PERSON_ID = P.PERSON_ID
where j.ID = 3 and interview.RULE = 1 and  j2.ROUND = interview.ROUND;

select job.ID,J.ID as jobCandID,job.JOBNAME,job.POSITION,job.ROUND as JOBROUND,job.NUMBERRECRUITMENT,P.FIRSTNAME, P.LASTNAME,P.EMAIL,p.PHONENUMBER,J.CREATEDATETIME,J2.ROUND as INTERVIEWROUND ,J2.STATUS, J.STATUS as JOBCANDSTATUS
from JOBS job
       join JOBCANDIDATES J on job.ID = J.JOB_ID
       join CANDIDATES C2 on J.CANDIDATE_ID = C2.ID
       join PERSONS P on C2.PERSON_ID = P.PERSON_ID
       join JOBCANDIDATEINTERVIEWS J2 on J.CURRENTROUND = J2.ID
where j2.STATUS = 'interview' and j.JOB_ID =2;

select ID, RULE, USER_ID, JOBCAND_ID
from INTERVIEWERJOBS where JOBCAND_ID = 3 and USER_ID = 1;


select *
from INTERVIEWERJOBS interview
       join JOBCANDIDATES jobCand on interview.JOBCAND_ID = jobCand.ID
       join JOBS J on jobCand.JOB_ID = J.ID;

-- select job by UserID and select candidate info with jobid and userID
select distinct job.ID, job.JOBNAME, job.POSITION,job.ROUND as JOBROUND, P.FIRSTNAME, P.LASTNAME, P.EMAIL,I.JOBCAND_ID, jobcan.CREATEDATETIME, JV.ROUND as JVROUND
from JOBS job
       join JOBCANDIDATES jobcan on jobcan.JOB_ID = job.ID
       join INTERVIEWERJOBS I on I.JOBCAND_ID = jobcan.ID
       join CANDIDATES C2 on jobcan.CANDIDATE_ID = C2.ID
       join PERSONS P on C2.PERSON_ID = P.PERSON_ID
       join JOBCANDIDATEINTERVIEWS JV on jobcan.ID = JV.JOBCAND_ID
where I.USER_ID = 2 and JV.STATUS = 'interview';
-- and jobcan.JOB_ID = 1;

select * from INTERVIEWERJOBS where USER_ID = 2;

-- load jobcandidate đã có sắp lịch phỏng vấn
select  job.ID, job.JOBNAME, job.POSITION, I.USER_ID, P.FIRSTNAME, P.LASTNAME, P.EMAIL,I.JOBCAND_ID, jobcan.CREATEDATETIME, J.ROUND,J.STATUS
from JOBS job
       join JOBCANDIDATES jobcan on jobcan.JOB_ID = job.ID
       join INTERVIEWERJOBS I on I.JOBCAND_ID = jobcan.ID
       join CANDIDATES C2 on jobcan.CANDIDATE_ID = C2.ID
       join PERSONS P on C2.PERSON_ID = P.PERSON_ID
       join JOBCANDIDATEINTERVIEWS J on jobcan.ID = J.JOBCAND_ID
where I.USER_ID = 2;

-- Thống kê Candidate trên giao diện Dashboard
select
  (select count(*)  from JOBCANDIDATES ) as total
     ,(select count(*) from JOBCANDIDATES where STATUS = 'waited') waited
     ,(select count(*) from JOBCANDIDATES where STATUS = 'viewed') viewed
     ,(select count(*) from JOBCANDIDATES where STATUS = 'interview') interview
     ,(select count(*) from JOBCANDIDATES where STATUS = 'offer') offer
from JOBCANDIDATES
where 1 = 1
  and CREATEDATETIME > sysdate -1
  and ROWNUM = 1;

-- Load data giao diện feedback

select J.JOBNAME, J.POSITION as P_RECRUITMENT
     , P.FIRSTNAME, P.LASTNAME, P.EMAIL, JCA.POSITION as P_APPY
     , JCAI.QUESTIONINTERVIEW,JCA.URLCV,JCAI.ROUND, JCAI.ADDRESS
     , TO_CHAR(JCAI.DATETIME,'DD-MM-YYYY HH24:MI:SS') as Date_Interview
from JOBCANDIDATES JCA
       join JOBS J on JCA.JOB_ID = J.ID
       join CANDIDATES CA on JCA.CANDIDATE_ID = CA.ID
       join PERSONS P on CA.PERSON_ID = P.PERSON_ID
       join JOBCANDIDATEINTERVIEWS  JCAI on JCA.CURRENTROUND = JCAI.ID
where JCA.ID = 9;

-- findResumeDetailByID
select CandType.NAME HEADER, CandDetail.NAME SUBHEADER, CandDetail.CONTENT CONTENT
from   CANDIDATES candi
       join CANDIDATETYPES CandType on candi.ID = CandType.CANDIDATE_ID
       join CANDIDATEDETAILS CandDetail on CandType.ID = CandDetail.CANDIDATETYPE_ID
where CANDIDATE_ID = 14;

-- findJobConfigDetail
select job.ID,JCF.CONTENT,CFG.CONFIGTYPE_ID,CFG.NAME,CFG_TP.NAME
from JOBS job
       join JOBCONFIGS JCF on job.ID = JCF.JOB_ID
       join CONFIGS CFG on JCF.CONFIG_ID = CFG.ID
       join CONFIGTYPES CFG_TP on CFG.CONFIGTYPE_ID = CFG_TP.ID
where JOB_ID = 7;

-- list jobcandidate apply by uset
select *
from  JOBCANDIDATES jobcan
       join INTERVIEWERJOBS I on I.JOBCAND_ID = jobcan.ID
       join CANDIDATES C2 on jobcan.CANDIDATE_ID = C2.ID
       join PERSONS P on C2.PERSON_ID = P.PERSON_ID
where I.USER_ID =5 ;