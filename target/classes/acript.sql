-- Person User
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Lương Ngọc Quyến', '12/03/2000','PT', 'Bão', '0379608838','ptbao@gmail.com','Male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Nguyễn Văn Trỗi', '12/05/1996','Trần', 'Tèo', '092344584','tranteo123@gmail.com','Female');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Trần Hưng Đạo', '21/04/1997','Lý', 'Tĩnh', '0371642145','lytinh456@gmail.com','Male');
--Person Candidate
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('789 Phan Xích Long', '12/03/1994','Huỳnh', 'Sơn', '0907555442','huynson111@gmail.com','Male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('1101 Quang Trung. Gò Vấp', '12/05/1996','Trần', 'Bùm', '0369635432','tranbum12@gmail.com','Female');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Hai Bà Trưng', '21/04/1997','Lưu', 'Hoàng', '0124222555','luuhoang199@gmail.com','Female');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('454 Huỳnh Khương An', '12/03/2010','Hồ', 'Quốc Vững', '0378825441','hovung@gmail.com','Male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Nguyễn Văn Cừ', '12/05/1996','Nguyễn', 'Thu Thảo', '092344584','thuthao123@gmail.com','Female');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Phan Văn Trị', '21/04/1997','Nguyễn', 'Văn Cường', '0371642145','vancuong1999@gmail.com','Male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Nguyên Văn Luong', '21/04/1997','Trân', 'Nguyên', '0124222555','trannguyen@gmail.com','Male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('789 Lương Ngoc Quyên', '12/03/1994','Pham', 'Vu Linh', '0907555442','vulinh@gmail.com','Female');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('1101 Phan Văn Trị', '12/05/1996','Trần', 'Chi Cương', '0369635432','chicuong@gmail.com','Male');
INSERT INTO persons(address,birthday,firstname,lastname,phonenumber,EMAIL,GENDER) VALUES ('123 Nguyễn Thị Minh Khai', '21/04/1997','Lưu', 'Thông', '0124222555','luuthong@gmail.com','Female');

-- User
-- Rule:
-- + 0: HR
-- + 1: Interview
-- + 2: HR Admin
INSERT INTO users (username, password, rule, person_id) VALUES ('alex123','$2a$04$4vwa/ugGbBVDvbWaKUVZBuJbjyQyj6tqntjSmG8q.hi97.xSdhj/2', 0, '1');
INSERT INTO users (username, password, rule, person_id)  VALUES ('tom234', '$2a$04$QED4arFwM1AtQWkR3JkQx.hXxeAk/G45NiRd3Q4ElgZdzGYCYKZOW', 1, '2');
INSERT INTO users (username, password, rule, person_id)  VALUES ('aadam', '$2a$04$WeT1SvJaGjmvQj34QG8VgO9qdXecKOYKEDZtCPeeIBSTxxEhazNla', 2, '3');

-- Candidate
INSERT INTO candidates(person_id) VALUES(4);
INSERT INTO candidates(person_id) VALUES(5);
INSERT INTO candidates(person_id) VALUES(6);
INSERT INTO candidates(person_id) VALUES(7);
INSERT INTO candidates(person_id) VALUES(8);
INSERT INTO candidates(person_id) VALUES(9);
INSERT INTO candidates(person_id) VALUES(10);
INSERT INTO candidates(person_id) VALUES(11);
INSERT INTO candidates(person_id) VALUES(12);
INSERT INTO candidates(person_id) VALUES(13);

-- Job
INSERT INTO JOBS(JOBDESCRIPTION,JOBNAME,POSITION,ROUND,NUMBERRECRUITMENT,CREATEDATETIME,UPDATEDATETIME)
VALUES ('Develop and implement machine learning application for real projects.Research, discuss and propose solutions for machine learning projects.Work with senior machine learning engineers and Professors in university from idea to development.Directly involve in building several current ML projects:SI Automation: extract information from scanned or image documents with various format.CCTV camera analysis in container terminal for safety and security.Chatbot development: develop chatbot application.	Develop machine learning application.',
        'Machine Learning Developer & Researcher',
        'Junior,Senior',
        3,
        20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO JOBS(JOBDESCRIPTION,JOBNAME,POSITION,ROUND,NUMBERRECRUITMENT,CREATEDATETIME,UPDATEDATETIME) VALUES ('Will be trained and work on Oracle, Java Spring, Action Script, HTML5, AngularJs.Join one of 2 teams: Product/ MaintenanceProduct: new development projectsMaintenance: the developed projects which related to the import and export process.The system has been operating in the US, Japan, Korea and some European countries, etc.After being clear about the purpose and meaning of the job, everyone will use the internal management system to manage and complete the work.The task will be done following the close procedure to ensure the high quality before reaching to the user through the internal system'
                                                                                                               ,'JAVA SOFTWARE ENGINEER','Entry,Junior, Senior',2,50,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO JOBS(JOBDESCRIPTION,JOBNAME,POSITION,ROUND,NUMBERRECRUITMENT,CREATEDATETIME,UPDATEDATETIME) VALUES ('Receive requests from world-wide customers via email, website, phone call.Support to resolve customer’s problems encountered when using the company''s system.Most of customers come from US, Korea, Japan, Singapore, India, China, Turkey, etc.'
                                                                                                               ,'BUSINESS ANALYSIS (GLOBAL HELPDESK)','HELPDESK',3,40,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- JobCandidate

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(1,1,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample1.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(1,2,'Junior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(1,3,'Junior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(2,3,'HELPDESK','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(2,2,'HELPDESK','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(3,1,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(3,3,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(4,1,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample1.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(4,2,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(4,3,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(5,2,'Junior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(5,1,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(5,3,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(6,3,'HELPDESK','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(6,2,'HELPDESK','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(6,1,'HELPDESK','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');


INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(7,1,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(7,3,'Senior','viewed',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample4.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(7,2,'Junior','viewed',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(8,1,'Senior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample1.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(8,2,'Junior','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample2.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(9,3,'HELPDESK','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(9,2,'HELPDESK','waited',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');

INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(10,2,'HELPDESK','viewed',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');
INSERT INTO JOBCANDIDATES(CANDIDATE_ID,JOB_ID,POSITION,STATUS,CURRENTROUND,IDINTERVIEWROUND,CREATEDATETIME,URLCV) VALUES(10,3,'HELPDESK','viewed',0,0,CURRENT_TIMESTAMP,'https://s3-us-west-2.amazonaws.com/recruitmentsystem/sample3.pdf');


-- JobCandidateInterview
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (3,1,'pass',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (4,1,'pass',CURRENT_TIMESTAMP,'Bad');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (4,2,'interview',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (6,1,'pass',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (6,2,'interview',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (8,1,'pass',CURRENT_TIMESTAMP,'Verry Good');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (8,2,'interview',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (8,2,'pass',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (9,1,'pass',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (9,2,'interview',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (11,1,'pass',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (16,1,'pass',CURRENT_TIMESTAMP,'Good');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (16,1,'pass',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (16,2,'interview',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (20,1,'pass',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (21,1,'pass',CURRENT_TIMESTAMP,'');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (18,1,'wait','','');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (19,1,'wait','','');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (24,1,'wait','','');
INSERT INTO JOBCANDIDATEINTERVIEWS(JOBCAND_ID,ROUND,STATUS,DATETIME,FEEDBACK) VALUES (25,1,'wait','','');

UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 1, STATUS = 'interview' WHERE t."ID" = 3;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 2, STATUS = 'interview' WHERE t."ID" = 4;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 3, STATUS = 'interview' WHERE t."ID" = 6;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 4, STATUS = 'interview' WHERE t."ID" = 8;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 5, STATUS = 'interview' WHERE t."ID" = 9;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 6, STATUS = 'interview' WHERE t."ID" = 11;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 7, STATUS = 'interview' WHERE t."ID" = 16;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 8, STATUS = 'interview' WHERE t."ID" = 20;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 9, STATUS = 'interview' WHERE t."ID" = 21;
-- status : viewed
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 10 WHERE t."ID" = 18;
UPDATE JOBCANDIDATES t SET t.CURRENTROUND = 11 WHERE t."ID" = 19;


-- Interview Job
-- User ID = 2: Interviewr -> tom234
INSERT  INTO INTERVIEWERJOBS(JOB_ID,USER_ID) values (1,2);
INSERT  INTO INTERVIEWERJOBS(JOB_ID,USER_ID) values (2,2);
-- CandidateType
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('EDUCATION',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('SKILLS',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('WORK EXPERIENCE',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('CERTIFICATES',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('LANGUAGE',1);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('INTERESTS',1);
-- INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('PROJECT',1);

INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('EDUCATION',2);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('SKILLS',2);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('WORK EXPERIENCE',2);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('LANGUAGE',2);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('INTERESTS',2);

INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('EDUCATION',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('SKILLS',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('WORK EXPERIENCE',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('CERTIFICATES',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('LANGUAGE',3);
INSERT INTO candidatetypes(name,CANDIDATE_ID) VALUES('INTERESTS',3);


-- CandidateDetail
-- Education 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"University":"DHCN","Major":"Software","GPA":"3.6/4","fromYear":"2010","toYear":"2015"}','Đại Học Công Nghiệp',1);
-- SKILLS 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"point":"2"}','JAVA',2);
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"point":"3"}','C#',2);
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"point":"4"}','JAVASCRIPT',2);
-- WORK EXPERIENCE 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"Company":"TMA Solution","Year":"3"}','JAVA',3);
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"Company":"Sacombank","Year":"1"}','Bank',3);
-- CERTIFICATES 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"Year":"2018","IssuedBy":"IUH","Point":"800",}','Toeic',4);
-- LANGUAGE 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{"Level":"1"}','English',5);
-- INTERESTS 1
INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{}','Reading Book',6);

INSERT INTO candidatedetails(content,name,CANDIDATETYPE_ID) VALUES('{}','Game',6);



INSERT INTO configtypes(name,content) VALUES('Technical','{"configtypes":[{"name":"Skill","properties":[{"type":"number","property":"Point","id":"1"}]},{"name":"WorkExperience","properties":[{"type":"number","property":"Year","id":"2"}]}]}');
INSERT INTO configtypes(name,content) VALUES('Bussiness','{"configtypes":[{"name":"TypeOfBussiness","properties":[{"type":"text","property":"nameBussiness","id":"1"},{"type":"number","property":"Year","id":"2"}]}]}');
INSERT INTO configtypes(name,content) VALUES('SoftwareSkill','{"configtypes":[{"name":"Languages","properties":[{"type":"text","property":"Language","id":"1"},{"type":"number","property":"level","id":"2"}]},{"name":"SoftSkill","properties":[{"type":"text","property":"nameSoft","id":"3"}]}]}');

-- Technical
INSERT INTO configs (name,configtype_id) VALUES('C#',1);
INSERT INTO configs (name,configtype_id) VALUES('JAVA',1);
INSERT INTO configs (name,configtype_id) VALUES('C++',1);

-- Bussiness
INSERT INTO configs (name,configtype_id) VALUES('Bank',2);
INSERT INTO configs (name,configtype_id) VALUES('Shipping Line',2);
INSERT INTO configs (name,configtype_id) VALUES('Insurrance',2);

-- Software Skill
INSERT INTO configs (name,configtype_id) VALUES('English',3);
INSERT INTO configs (name,configtype_id) VALUES('Communication',3);
INSERT INTO configs (name,configtype_id) VALUES('Research',3);
INSERT INTO configs (name,configtype_id) VALUES('Toeic',3);

-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,1,'{}');
-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,1,'{}');
-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,1,'{}');
-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,2,'{}');
-- Insert into JOBCONFIGS (CONFIG_ID,JOB_ID,CONTENT) values (0,3,'{}');

alter table configtypes add constraint configtypes_json_chk CHECK (CONTENT IS JSON) ENABLE NOVALIDATE;
alter table JOBCONFIGS add constraint JOBCONFIGS_json_chk CHECK (CONTENT IS JSON) ENABLE NOVALIDATE;
alter table CANDIDATEDETAILS add constraint CANDIDATEDETAILS_json_chk CHECK (CONTENT IS JSON) ENABLE NOVALIDATE;

-- select p.FIRSTNAME,p.LASTNAME,p.ADDRESS,p.BIRTHDAY,p.PHONENUMBER, L.CHANGEVALUE,L.TOVALUE,L.CREATEDATETIME
--   from CANDIDATES can join PERSONS P on can.PERSON_ID = P.PERSON_ID
--     join LOGS L on can.ID = L.CANDIDATE_ID
--
-- select * from JOBCONFIGS cfg  where cfg.JOB_ID = 21
--
-- select job.JOBNAME from JOBS job join JOBCANDIDATES jcnd on job.ID = jcnd.JOB_ID
--       join JOBCANDIDATEINTERVIEWS jcin  on jcnd.ID = jcin.JOBCAND_ID
-- where job.ID = 3 and ( jcnd.STATUS = 'viewed' or jcnd.STATUS='interviewing')

select job.ID,job.JOBNAME,job.POSITION,job.ROUND as JOBROUND,job.NUMBERRECRUITMENT,P.FIRSTNAME, P.LASTNAME,P.EMAIL,p.PHONENUMBER,J.CREATEDATETIME,J2.ROUND as INTERVIEWROUND ,J2.STATUS, J.STATUS as JOBCANDSTATUS
from JOBS job
       join JOBCANDIDATES J on job.ID = J.JOB_ID
       join CANDIDATES C2 on J.CANDIDATE_ID = C2.ID
       join PERSONS P on C2.PERSON_ID = P.PERSON_ID
       join JOBCANDIDATEINTERVIEWS J2 on J.CURRENTROUND = J2.ID
where job.ID = 1